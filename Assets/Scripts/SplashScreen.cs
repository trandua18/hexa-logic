﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SplashScreen : MonoBehaviour {
    public static SplashScreen Instance;

    public GameObject gLoading;
    public int maxLevel;

    private AsyncOperation asyncOperation;
    private GameObject gLoadingPre;


    private void Awake()
    {
        //Debug.LogError(PlayerPrefs.GetString(GameUtils.TIME_ADDED_LIFE_KEY) + " vs " + GameUtils.GetDurationMinute() + " vs " + GameUtils.GetStringDateTime(GameUtils.GetDurationMinute()));
        int lifeAdded = (int)(GameUtils.GetDurationMinute() / 20);
        GameUtils.AddLife(lifeAdded);
        GameUtils.PutDateTime(System.DateTime.Now);

        if (Instance == null)
        {
            DontDestroyOnLoad(this);
            Instance = this;
        }
        else Destroy(gameObject);

    }
    private void OnEnable()
    {
        if (!GameUtils.GetFinishTut())
        {
            GameUtils.ResetTutStep();
        }
    }

    private void Start()
    {
        LoadSene(GameUtils.SCENE_START);
    }


    IEnumerator Loading()
    {
        while (asyncOperation.progress < 0.9f)
        {
            gLoadingPre.transform.GetChild(0).GetChild(0).GetChild(0).GetChild(0).gameObject.GetComponent<Image>().fillAmount = asyncOperation.progress;
            yield return null;
        }
        asyncOperation.allowSceneActivation = true;
    }

    public void LoadSene(string sceneName)
    {
        asyncOperation = SceneManager.LoadSceneAsync(sceneName);
        asyncOperation.allowSceneActivation = false;
        gLoadingPre = Instantiate<GameObject>(gLoading);

        StartCoroutine(Loading());
    }
}
