﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class HexaEditor : MonoBehaviour {

    [MenuItem("HexaMenu/Delete all key")]
    static void DeleteAllPrefs()
    {
        PlayerPrefs.DeleteAll();
    }
    [MenuItem("HexaMenu/Delete PrefsLevel")]
    static void DeletePrefsLevel()
    {
        PlayerPrefs.DeleteKey(GameUtils.CURRENT_MAP);
    }
}
