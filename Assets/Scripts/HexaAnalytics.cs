﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Analytics;
using Facebook.Unity;

public class HexaAnalytics : MonoBehaviour {

    public static HexaAnalytics Instance;

    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
    protected bool firebaseInitialized = false;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start() {
        InitFirebase();
        InitFacebook();
    }

    #region Game Events
    #region Event Tutorial
    public void EVTutBegin()
    {
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventTutorialBegin);
        FB.LogAppEvent(FirebaseAnalytics.EventTutorialBegin);
    }
    public void EVTutComplete()
    {
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventTutorialComplete);
        FB.LogAppEvent(FirebaseAnalytics.EventTutorialComplete);
    }
    #endregion

    #region Event Level
    public void EVLevelStart(int level) {
        Parameter[] pams = new Parameter[] { new Parameter(FirebaseAnalytics.ParameterLevel, level)};
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelStart, pams);

        Dictionary<string, object> dicParam = new Dictionary<string, object>();
        dicParam.Add(FirebaseAnalytics.ParameterLevel, level);
        FB.LogAppEvent(FirebaseAnalytics.EventLevelStart, null, dicParam);
    }
    public void EVLevelEnd(int level) {
        Parameter[] pams = new Parameter[] { new Parameter(FirebaseAnalytics.ParameterLevel, level) };
        FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelEnd, pams);

        Dictionary<string, object> dicParam = new Dictionary<string, object>();
        dicParam.Add(FirebaseAnalytics.ParameterLevel, level);
        FB.LogAppEvent(FirebaseAnalytics.EventLevelEnd, null, dicParam);
    }
    #endregion

    #region Buying InApp
    public void EVStartBuyIAP(string sku, string location, int value, int playerLevel) {
        Parameter[] pams = new Parameter[] { new Parameter(FirebaseAnalytics.ParameterItemId, sku), new Parameter(FirebaseAnalytics.ParameterItemLocationId, location), new Parameter(FirebaseAnalytics.ParameterValue, value),
            new Parameter(FirebaseAnalytics.ParameterLevel, playerLevel) };
        FirebaseAnalytics.LogEvent("start_inapp_purchase", pams);

        Dictionary<string, object> dicParam = new Dictionary<string, object>();
        dicParam.Add(FirebaseAnalytics.ParameterItemId, sku);
        dicParam.Add(FirebaseAnalytics.ParameterItemLocationId, location);
        dicParam.Add(FirebaseAnalytics.ParameterValue, value);
        dicParam.Add(FirebaseAnalytics.ParameterLevel, playerLevel);
        FB.LogAppEvent("start_inapp_purchase", null, dicParam);
    }
    public void EVFinishBuyIAP(string sku, string location, double value, int playerLevel) {
        Parameter[] pams = new Parameter[] { new Parameter(FirebaseAnalytics.ParameterItemId, sku), new Parameter(FirebaseAnalytics.ParameterItemLocationId, location), new Parameter(FirebaseAnalytics.ParameterValue, value),
            new Parameter(FirebaseAnalytics.ParameterLevel, playerLevel) };
        FirebaseAnalytics.LogEvent("finished_inapp_purchase", pams);

        Dictionary<string, object> dicParam = new Dictionary<string, object>();
        dicParam.Add(FirebaseAnalytics.ParameterItemId, sku);
        dicParam.Add(FirebaseAnalytics.ParameterItemLocationId, location);
        dicParam.Add(FirebaseAnalytics.ParameterValue, value);
        dicParam.Add(FirebaseAnalytics.ParameterLevel, playerLevel);
        FB.LogAppEvent("finished_inapp_purchase", null, dicParam);
    }
    #endregion

    #region Start Watch Rewarded Video
    public void EVStartWatchVideo(string locationID) {
        Parameter[] pams = new Parameter[] { new Parameter(FirebaseAnalytics.ParameterItemLocationId, locationID) };
        FirebaseAnalytics.LogEvent("start_rewarded_video", pams);

        Dictionary<string, object> dicParam = new Dictionary<string, object>();
        dicParam.Add(FirebaseAnalytics.ParameterItemLocationId, locationID);
        FB.LogAppEvent("start_rewarded_video", null, dicParam);
    }
    public void EVFinishWatchVideo(string locationID, string rewardItemType, int rewardValue) {
        Parameter[] pams = new Parameter[] { new Parameter(FirebaseAnalytics.ParameterItemLocationId, locationID),
         new Parameter(FirebaseAnalytics.ParameterItemId, rewardItemType),
         new Parameter(FirebaseAnalytics.ParameterValue, rewardValue)};
        FirebaseAnalytics.LogEvent("finished_rewarded_video", pams);

        Dictionary<string, object> dicParam = new Dictionary<string, object>();
        dicParam.Add(FirebaseAnalytics.ParameterItemLocationId, locationID);
        dicParam.Add(FirebaseAnalytics.ParameterItemId, rewardItemType);
        dicParam.Add(FirebaseAnalytics.ParameterValue, rewardValue);
        FB.LogAppEvent("finished_rewarded_video", null, dicParam);
    }
    #endregion
    #region Using item InGame
    public void EVUsingItem()
    {
        Parameter[] pams = new Parameter[] { new Parameter("hint_count", 1) };
        FirebaseAnalytics.LogEvent("using_hint", pams);

        Dictionary<string, object> dicParam = new Dictionary<string, object>();
        dicParam.Add("hint_count", 1);
        FB.LogAppEvent("using_hint", null, dicParam);
    }
    #endregion
    #endregion

    #region Init
    private void InitFirebase() {
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }
    void InitializeFirebase()
    {
        FirebaseAnalytics.SetUserProperty(FirebaseAnalytics.UserPropertySignUpMethod, "Google");
        firebaseInitialized = true;
    }
    void InitFacebook() {
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();
        }
    }
    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }
    #endregion
}