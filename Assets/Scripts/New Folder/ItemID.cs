﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemID : MonoBehaviour
{

    public int idItem;
    public int posX, posY;
    // Use this for initialization
    void Start()
    {
        CheckColor();
    }
    public void CheckColor()
    {
        switch (idItem)
        {
            case 1:
                GetComponent<SpriteRenderer>().color = Color.yellow;
                break;
            case 2:
                GetComponent<SpriteRenderer>().color = Color.gray;
                break;
            case 3:
                GetComponent<SpriteRenderer>().color = Color.red;
                break;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnMouseDown()
    {
        Find(posX, posY, idItem);
        print(lstPos.Count);
        //print(posX + ", " + posY + ", " + idItem);
        if (lstPos.Count > 1)
        {
            idItem += 1;
            CheckColor();
        }
    }
    private void OnMouseUp()
    {
        lstPos.Clear();
    }
    List<Vector2> lstPos = new List<Vector2>();
    void Find(int x, int y, int value)
    {
        Vector2 _it = new Vector2();
        _it.x = x;
        _it.y = y;
        if (!lstPos.Contains(_it))
        {
            //print(lstItem.Count);
            if (x >= 0 && y >= 0 && x < Board.Instance.iids.GetLength(0) && y < Board.Instance.iids.GetLength(1))
            {
                if (Board.Instance.iids[x, y] == value)
                {

                    Debug.Log("" + x + "," + y + ": " + value);

                    lstPos.Add(_it);
                    Find(x + 1, y, value);
                    Find(x, y + 1, value);
                    Find(x - 1, y, value);
                    Find(x, y - 1, value);
                    print(_it);

                }
            }
        }
    }
}
