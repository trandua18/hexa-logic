﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    private int idItem;
    public int width;
    public int height;
    public GameObject gItem;
    public Transform parent;
    public List<ItemID> lstItem = new List<ItemID>();
    public int[,] iids;

    public static Board Instance;

    private void Awake()
    {
        Instance = this;
    }
    // Use this for initialization
    void Start()
    {
        InitBoard();
    }
    private void InitBoard()
    {
        iids = new int[height, width];
        for (int i = 0; i < /*iids.GetLength(0)*/width; i++)
        {
            for (int j = 0; j < /*iids.GetLength(1)*/height; j++)
            {
                idItem = Random.Range(1, 3);
                GameObject g = Instantiate(gItem, new Vector3(i - (width - 1) / 2f, j - (height - 1) / 2f), Quaternion.identity) as GameObject;
                g.GetComponent<ItemID>().idItem = idItem;
                g.GetComponent<ItemID>().posX = i;
                g.GetComponent<ItemID>().posY = j;
                g.transform.SetParent(parent, false);
                iids[i, j] = idItem;
            }
        }


        //Show Value
        for (int i = 0; i < iids.GetLength(0); i++)
        {
            for (int j = 0; j < iids.GetLength(1); j++)
            {
                Debug.Log(i + "," + j + " : " + iids[i, j]);
            }
        }
    }
}