﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdsManager : MonoBehaviour {

    public static AdsManager Instance;
    private InterstitialAd interstitial;
    private RewardBasedVideoAd rewardBasedVideo;
    private BannerView bannerView;



    public Action acInterstitialClose;
    public Action acRewarded;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else {
            Destroy(gameObject);
        }
    }
    public bool IsInterstitialAvaiable() {
        return interstitial.IsLoaded();
    }
    public bool IsVideoAvaiable() {
        return rewardBasedVideo.IsLoaded();
    }
    public void ShowReward() {
        if (IsVideoAvaiable() && !GameUtils.GetNoAds())
        {
            rewardBasedVideo.Show();
            HexaAnalytics.Instance.EVStartWatchVideo("main_game_screen");
        }
    }
    public void ShowInterstitial() {
        if (IsInterstitialAvaiable() && !GameUtils.GetNoAds()) interstitial.Show();
    }
    private void LoadInterstitial() {
        interstitial = new InterstitialAd(GameUtils.ID_INTERSTITIAL);
        interstitial.OnAdLoaded += HandleOnAdLoaded;
        // Called when an ad request failed to load.
        interstitial.OnAdFailedToLoad += HandleOnAdFailedToLoad;
        // Called when an ad is shown.
        interstitial.OnAdOpening += HandleOnAdOpened;
        // Called when the ad is closed.
        interstitial.OnAdClosed += HandleOnAdClosed;
        // Called when the ad click caused the user to leave the application.
        interstitial.OnAdLeavingApplication += HandleOnAdLeavingApplication;

        interstitial.LoadAd(MyRequest());
    }
    #region Handle Interstitial Event
    public void HandleOnAdLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLoaded event received");
    }

    public void HandleOnAdFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleFailedToReceiveAd event received with message: " + args.Message);
    }

    public void HandleOnAdOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdOpened event received");
    }

    public void HandleOnAdClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdClosed event received");
        if (acInterstitialClose != null) acInterstitialClose();
        interstitial.LoadAd(MyRequest());
    }

    public void HandleOnAdLeavingApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleAdLeavingApplication event received");
    }
    #endregion

    private AdRequest MyRequest() {
        return new AdRequest.Builder().AddExtra("npa", "1").Build();
    }
    private void LoadRewardVideo() {
        rewardBasedVideo.LoadAd(MyRequest(), GameUtils.ID_REWARDED_VIDEO);
    }
    // Use this for initialization
    void Start () {
        MobileAds.Initialize(GameUtils.ID_PUBLISHER);

        RequestBanner();

        LoadInterstitial();
        rewardBasedVideo = RewardBasedVideoAd.Instance;
        // Called when an ad request has successfully loaded.
        rewardBasedVideo.OnAdLoaded += HandleRewardBasedVideoLoaded;
        // Called when an ad request failed to load.
        rewardBasedVideo.OnAdFailedToLoad += HandleRewardBasedVideoFailedToLoad;
        // Called when an ad is shown.
        rewardBasedVideo.OnAdOpening += HandleRewardBasedVideoOpened;
        // Called when the ad starts to play.
        rewardBasedVideo.OnAdStarted += HandleRewardBasedVideoStarted;
        // Called when the user should be rewarded for watching a video.
        rewardBasedVideo.OnAdRewarded += HandleRewardBasedVideoRewarded;
        // Called when the ad is closed.
        rewardBasedVideo.OnAdClosed += HandleRewardBasedVideoClosed;
        // Called when the ad click caused the user to leave the application.
        rewardBasedVideo.OnAdLeavingApplication += HandleRewardBasedVideoLeftApplication;


        LoadRewardVideo();
    }

    #region Banner
    private void RequestBanner()
    {
        bannerView = new BannerView(GameUtils.ID_BANNER, AdSize.Banner, AdPosition.Bottom);

        // Called when an ad request has successfully loaded.
        bannerView.OnAdLoaded += BannerView_OnAdLoaded;
        
        bannerView.LoadAd(MyRequest());
    }

    private void BannerView_OnAdLoaded(object sender, EventArgs e)
    {
        HideBanner();
    }

    public void ShowBanner() {
        bannerView.Show();
    }
    public void HideBanner() {
        bannerView.Hide();
    }
    #endregion


    
    #region Handle Rewarded Video Event
    public void HandleRewardBasedVideoLoaded(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLoaded event received");
    }

    public void HandleRewardBasedVideoFailedToLoad(object sender, AdFailedToLoadEventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoFailedToLoad event received with message: "+ args.Message);
    }

    public void HandleRewardBasedVideoOpened(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoOpened event received");
    }

    public void HandleRewardBasedVideoStarted(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoStarted event received");
    }

    public void HandleRewardBasedVideoClosed(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoClosed event received");
        LoadRewardVideo();
    }

    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print("HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);
        if (acRewarded != null) {
            acRewarded();
        }
    }

    public void HandleRewardBasedVideoLeftApplication(object sender, EventArgs args)
    {
        MonoBehaviour.print("HandleRewardBasedVideoLeftApplication event received");
    }

    #endregion
}
