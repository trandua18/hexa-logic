﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;

public class StoreManager : MonoBehaviour, IStoreListener
{

    public static StoreManager Instance;
    public IStoreController m_StoreController;

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        m_StoreController = controller;
        Debug.LogError("OnInitialized");
    }

    public void OnInitializeFailed(InitializationFailureReason error)
    {
        Debug.LogError("OnInitializeFailed: " + error);
    }

    public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
    {
        Debug.LogError("OnPurchaseFailed__ " + i.availableToPurchase);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    {
        Debug.LogError("OnInitialized");
        return PurchaseProcessingResult.Complete;
    }
    public string pdNoAds, pdBuyHint, pdTurn, pdLife;
    private void Awake()
    {
        DontDestroyOnLoad(this);
        Instance = this;

        pdNoAds = "com.hexologic.noads";
        pdBuyHint = "com.hexologic.buyhints";
        pdTurn = "com.hexologic.extraturn";
        pdLife = "com.hexologic.extralife";

        ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(pdBuyHint, ProductType.Consumable);
        builder.AddProduct(pdLife, ProductType.Consumable);
        builder.AddProduct(pdNoAds, ProductType.Consumable);
        builder.AddProduct(pdTurn, ProductType.Consumable);
        UnityPurchasing.Initialize(this, builder);
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
