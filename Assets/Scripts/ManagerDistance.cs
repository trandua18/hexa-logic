﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class ManagerDistance : MonoBehaviour {
    public List<Transform> lstAllObject = new List<Transform>();

    public static ManagerDistance Instance;
    private void Awake()
    {
        if (Instance == null) {
            Instance = this;
        }
    }
    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        foreach (Transform _tr in lstAllObject)
        {
            float distance = Vector3.Distance(transform.position, _tr.position);
            if (distance <= 9)
            {
                _tr.gameObject.SetActive(true);
            }
            else
            {
                _tr.gameObject.SetActive(false);
            }
        }
    }
}
