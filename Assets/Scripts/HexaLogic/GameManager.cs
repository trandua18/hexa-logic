﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DoozyUI;
using System.IO;

public class GameManager : MonoBehaviour
{
    public static GameManager THIS;

    public GameObject gPanelMainPlay;
    public GameObject gPanelComplete;
    public GameObject gPanelPause;
    public GameObject gPanelQuit;
    public GameObject gPanelGrid;
    public GameObject gPanelGameOver;
    public Button btnSound;
    public Sprite sprSoundOn, sprSoundOff, sprStarY;
    public GameObject gImageInfo;
    public GameObject gStarsHint;
    public GameObject gAllStars;
    public GameObject gStar_Anim, gFirework1, gFirework2, gConfentti;
    public GameObject gNoti;
    public GameObject gTutorial;
    public GameObject gHandTut;
    public Transform trStarsGameOver;

    public Text txtNoti;
    public Text txtAllStars;
    public Text txtHints;
    public Text txtQuitGameTitle;
    public Text txtQuitConfirm;
    public Text txtCongrattulation;
    public Text txtParam1, txtParam2, txtParam3, txtTarget, txtSuccess;
    public Text txtHintTitle, txtHintContent;
    public Text txtInfo;
    public Text txtTurn;
    public Text txtLife;
    public GameObject btnPause;
    public GameObject gParticle;
    public GameObject gNote1, gNote2, gNote3;
    public GameObject gPanelBuyHint;
    public GameObject gPanelShop;
    public GameObject gGameGUI;
    public float timeSwitchNote;
    [HideInInspector]
    public Vector3 vStarEffPos;
    public bool isUseHint;
    public bool isQuitGame, isGamePause, isGameOver;
    public int maxMove = 0, curMove = 0;
    public List<GameObject> lstTutStep = new List<GameObject>();


    private float timeCounterNoti = 0;
    private float timeDisapear = 0;
    private bool isStarMove;
    private Vector3 vDes;
    private Hexo _hexo;

    private Color clButtonPause;
    public System.Action<bool> _acSoundHanler;

    public void ShowClickEffect(GameObject gSelected)
    {
        StartCoroutine(ShowParticle(gSelected));
    }
    private IEnumerator ShowParticle(GameObject gSelected)
    {
        gParticle.transform.position = gSelected.transform.position;
        gParticle.SetActive(true);
        yield return new WaitForSeconds(0.5f);
        gParticle.SetActive(false);
    }
    private void HandleInterstitialClose()
    {
    }
    private void HandleRewarded()
    {
        GameUtils.SetTotalHints(+1);
        txtHints.text = GameUtils.GetTotalHints() + "";
        HexaAnalytics.Instance.EVFinishWatchVideo("main_game_screen", "hints", 1);
    }

    public void UpdatePlayerTurn()
    {
        txtTurn.text = curMove + "/" + maxMove;
    }
    private void Awake()
    {
        THIS = this;
    }
    private void OnEnable()
    {
        txtQuitConfirm.text = GameUtils.dicLangInUse[GameUtils.ARE_YOU_SURE];
        txtQuitGameTitle.text = GameUtils.dicLangInUse[GameUtils.QUIT_GAME].ToUpper();
        txtCongrattulation.text = GameUtils.dicLangInUse[GameUtils.CONGRA].ToUpper();
        txtHintTitle.text = GameUtils.dicLangInUse[GameUtils.BUY_HINT_TITLE].ToUpper();
        txtHintContent.text = GameUtils.dicLangInUse[GameUtils.BUY_MORE_HINT];
        txtInfo.text = GameUtils.dicLangInUse[GameUtils.INFO_KEY].ToUpper();
        txtLife.text = GameUtils.GetLife() + "";

        if (GameUtils.GetFinishTut())
        {
            if (GameUtils.isPlayTutorialMap) gTutorial.SetActive(true);
            else
                gTutorial.SetActive(false);
        }
        else
        {
            gTutorial.SetActive(true);
        }

        AdsManager.Instance.acInterstitialClose += HandleInterstitialClose;
        AdsManager.Instance.acRewarded += HandleRewarded;
    }
    public void CallBuyMoreHint()
    {
        if (AdsManager.Instance.IsVideoAvaiable()) AdsManager.Instance.ShowReward();
        else
        {
            gPanelShop.SetActive(true);
        }
    }

    public void CheckPlayerMove()
    {
        curMove++;
        if (curMove > maxMove)
        {
            GameUtils.ShowElement(gPanelGameOver);
            GameUtils.AddLife(-1);
            txtLife.text = GameUtils.GetLife() + "";
            isGameOver = true;
        }
        UpdatePlayerTurn();
        CheckTurn();
    }
    public void HideButtonPause(bool isHide)
    {
        btnPause.GetComponent<Button>().interactable = isHide;
        Color _cl = new Color(clButtonPause.r, clButtonPause.g, clButtonPause.b, isHide ? 1 : 0);
        btnPause.GetComponent<Image>().color = _cl;
        gGameGUI.SetActive(isHide);
    }
    private void Start()
    {
        if (GameUtils.GetSound())
        {
            btnSound.GetComponent<Image>().sprite = sprSoundOn;
        }
        else
        {
            btnSound.GetComponent<Image>().sprite = sprSoundOff;
        }
        clButtonPause = btnPause.GetComponent<Image>().color;
        vStarEffPos = gStarsHint.transform.position;

        ShopController.Instance.acAddHint += HandleBuyHintEvent;
        ShopController.Instance.acAddLife += HandleBuyLife;
        ShopController.Instance.acAddTurnLimit += HandleBuyExtraTurn;
    }


    private void HandleBuyHintEvent(int _total)
    {
        GameUtils.SetTotalHints(_total);

        txtHints.text = GameUtils.GetTotalHints() + "";
    }
    private void HandleBuyExtraTurn()
    {
        GameUtils.SetBuyExtraTurn();

        int turnExtra = 0;
        if (GameUtils.HasBuyExtraTurn())
        {
            turnExtra = maxMove + (int)(maxMove * 0.5f);
        }
        maxMove = maxMove + turnExtra;
        txtTurn.text = curMove + "/" + maxMove;
    }
    private void HandleBuyLife(int _total)
    {
        GameUtils.AddLife(_total);
        txtLife.text = GameUtils.GetLife() + "";
    }
    private bool isCheckAddLife = false;
    IEnumerator CheckAddedLife()
    {
        yield return GameUtils.GetDurationMinute() >= 20;
        GameUtils.PutDateTime(System.DateTime.Now);
        int lifeAdded = (int)(GameUtils.GetDurationMinute() / 20);
        GameUtils.AddLife(lifeAdded);
        txtLife.text = GameUtils.GetLife() + "";
    }


    public void UpdateAllStars()
    {
        txtAllStars.text = GameUtils.GetTotalStars() + "";
        txtHints.text = GameUtils.GetTotalHints() + "";
    }
    public int CalculateStar()
    {
        int _star = 2;
        if (curMove <= maxMove)
        {
            _star = 3;
        }
        return _star;
    }
    private void Update()
    {
        if (GameUtils.GetDurationMinute() == 20)
        {
            int lifeAdded = (int)(GameUtils.GetDurationMinute() / 20);
            GameUtils.AddLife(lifeAdded);
            txtLife.text = GameUtils.GetLife() + "";
            GameUtils.PutDateTime(System.DateTime.Now);
        }

        #region Hint Star Effects
        if (isStarMove)
        {
            gStarsHint.transform.position = Vector3.MoveTowards(gStarsHint.transform.position, vDes, 10 * Time.deltaTime);
            if (Vector3.Distance(gStarsHint.transform.position, vDes) <= 0.1f)
            {
                timeDisapear += Time.deltaTime;
                if (timeDisapear >= 1)
                {
                    gStarsHint.SetActive(false);
                    isStarMove = false;
                    _hexo.SetDotActiveByValue(_hexo.TargetValue);

                    HexoInfo hInfo = new HexoInfo();
                    hInfo.IdX = _hexo.IdX;
                    hInfo.IdY = _hexo.IdY;
                    hInfo.IsLocked = _hexo.IsLocked;
                    hInfo.keys.AddRange(_hexo.keys);
                    hInfo.myKey = _hexo.myKey;
                    hInfo.TargetValue = _hexo.TargetValue;
                    hInfo.CurrentValue = _hexo.TargetValue;
                    hInfo.DependentId = _hexo.DependentId;
                    hInfo.HintsData = _hexo.hintsData;
                    MapCreate.Instance.Search(hInfo);

                    timeDisapear = 0;
                    gStarsHint.transform.position = vStarEffPos;
                }
            }
        }
        #endregion

        if (gNoti.activeSelf)
        {
            timeCounterNoti += Time.deltaTime;
            if (timeCounterNoti >= 1.5f)
            {
                gNoti.SetActive(false);
                timeCounterNoti = 0;
            }
        }


        if (gImageInfo.activeSelf)
        {
            timeCounterNoti += Time.deltaTime;
            if (timeCounterNoti >= 3.5f)
            {
                gImageInfo.SetActive(false);
                timeCounterNoti = 0;
            }
        }
    }

    public void CheckTurn()
    {
        if (curMove == MapCreate.Instance._totalHexValue)
        {
            //SoundManager.THIS.PlaySound(SoundManager.THIS.acWarningTurnLimit);
            txtTurn.GetComponent<Animator>().Play("TextTurnWarning");
        }
        if (curMove > MapCreate.Instance._totalHexValue)
        {
            txtTurn.GetComponent<Animator>().Play("TextTurnLimit");
        }
    }

    #region Hints
    public void CheckUsingHint(GameObject gSelected)
    {
        isStarMove = true;
        vDes = gSelected.transform.position;
        _hexo = gSelected.GetComponent<Hexo>();
    }
    public void UseHint()
    {
        if (MapCreate.Instance.IsWinGame()) return;

        if (!gStarsHint.activeSelf)
        {
            if (GameUtils.GetTotalHints() >= 1)
            {
                SoundManager.THIS.PlaySound(SoundManager.THIS.acHintClick);
                gStarsHint.SetActive(true);
                GameUtils.SetTotalHints(-1);
                HexaAnalytics.Instance.EVUsingItem();
                isUseHint = true;
                UpdateAllStars();
            }
            else
            {
                GameUtils.ShowElement(gPanelBuyHint);
            }
        }
    }
    #endregion
    public void SetSound()
    {
        GameUtils.SetSound();
        if (GameUtils.GetSound())
        {
            btnSound.GetComponent<Image>().sprite = sprSoundOn;
            if (_acSoundHanler != null) _acSoundHanler(true);
        }
        else
        {
            btnSound.GetComponent<Image>().sprite = sprSoundOff;
            if (_acSoundHanler != null) _acSoundHanler(false);
        }
    }

    public void ShowLevelComplete()
    {
        SoundManager.THIS.StopBackGroundMusic();
        SoundManager.THIS.PlaySound(SoundManager.THIS.acWin);
        GameUtils.totalLevelComplete += 1;

        HexaAnalytics.Instance.EVLevelEnd(GameUtils.curLevel);

        if (!GameUtils.GetFinishTut()) GameUtils.SetFinishTut();
        if (GameUtils.isPlayTutorialMap || !GameUtils.GetFinishTut())
        {
            txtParam1.gameObject.SetActive(false);
            txtParam2.gameObject.SetActive(false);
            txtParam3.gameObject.SetActive(false);
            txtTarget.gameObject.SetActive(false);
            txtSuccess.text = GameUtils.dicLangInUse[GameUtils.SUCCESSFUL_KEY].ToUpper();
            txtSuccess.gameObject.SetActive(true);

            GameUtils.isPlayTutorialMap = false;
        }
        MenuController.THIS.SmoothMove(0.5f);
        StartCoroutine(ShowComplete());
    }
    private void OnDisable()
    {
        ShopController.Instance.acAddHint -= HandleBuyHintEvent;
        ShopController.Instance.acAddLife -= HandleBuyLife;
        ShopController.Instance.acAddTurnLimit -= HandleBuyExtraTurn;


        AdsManager.Instance.acInterstitialClose -= HandleInterstitialClose;
        AdsManager.Instance.acRewarded -= HandleRewarded;
    }
    IEnumerator ShowComplete()
    {
        yield return new WaitForSeconds(1.0f);
        GameUtils.HideElement(gPanelGrid);
        yield return new WaitForSeconds(1.0f);
        txtSuccess.gameObject.SetActive(false);
        gStar_Anim.SetActive(true);
        SoundManager.THIS.PlaySound(SoundManager.THIS.acFirework);
        starFxController.myStarFxController.ea = CalculateStar();
        yield return new WaitForSeconds(1.0f);
        gFirework2.SetActive(true);
        yield return new WaitForSeconds(1.0f);
        gFirework1.SetActive(true);
        GameUtils.ShowElement(gPanelComplete);
        gConfentti.SetActive(true);

        if (GameUtils.totalLevelComplete == 3)
        {
            AdsManager.Instance.ShowInterstitial();
            GameUtils.totalLevelComplete = 0;
        }

        yield return new WaitForSeconds(4.0f);
        gFirework1.SetActive(false);
        gConfentti.SetActive(false);
        gFirework2.SetActive(false);

        TakeScreenShot();
    }

    public void CallResumeGame()
    {
        HideButtonPause(true);
        isGamePause = false;
    }
    public void SoundToggle()
    {

    }
    public void CallRestartGame()
    {
        if (GameUtils.GetLife() > 0)
        {
            MenuController.THIS.gGamePlay.SetActive(false);
            GameUtils.isGameRestart = true;
            CallShowHomeGame();

            if (GameUtils.isGameRestart)
            {
                HexaAnalytics.Instance.EVTutBegin();

                GameUtils.isGameRestart = false;
                MenuController.THIS.InitStartGame(GameUtils.curLevel);
            }
        }
        else
        {
            GameManager.THIS.gPanelShop.SetActive(true);
        }


    }
    public void CloseCompletePanel()
    {
        CallShowHomeGame();
    }
    public void CallShowHomeGame()
    {
        isGamePause = false;
        isGameOver = false;
        MapCreate.Instance.ClearCache();

        GameUtils.HideElement(gPanelPause);
        GameUtils.HideElement(gPanelGrid);
        GameUtils.HideElement(gPanelGameOver);
        HideButtonPause(true);
        MenuController.THIS.scRect.enabled = true;
        MenuController.THIS.gMapLevels.SetActive(true);
        MenuController.THIS.gRingCurrentMap.SetActive(true);

        ///TODO CloseComplete
        gStar_Anim.SetActive(false);
        gFirework2.SetActive(false);
        gFirework1.SetActive(false);
        GameUtils.HideElement(gPanelComplete);
        gConfentti.SetActive(false);
        ResetStars();
    }

    public void CallShowPausePanel()
    {
        if (MapCreate.Instance.IsWinGame()) return;
        isGamePause = true;
        GameUtils.ShowElement(gPanelPause);
        HideButtonPause(false);
    }
    public void CallShowQuitPanel()
    {
        isQuitGame = true;
        gPanelMainPlay.SetActive(false);

        UIElement _uElementQuit = gPanelQuit.GetComponent<UIElement>();
        GameUtils.ShowElement(gPanelQuit);
    }

    public void HideAllPanel()
    {
        gPanelMainPlay.SetActive(true);
        GameUtils.HideElement(gPanelQuit);
        GameUtils.HideElement(gPanelComplete);
    }
    private void HandleNextButton()
    {
        CallShowHomeGame();
        GameUtils.HideElement(gPanelGrid);
        gStar_Anim.SetActive(false);
        gFirework2.SetActive(false);
        gFirework1.SetActive(false);
        GameUtils.HideElement(gPanelComplete);
        gConfentti.SetActive(false);
    }

    public void ResetStars()
    {
        for (int i = 0; i < gStar_Anim.transform.childCount; i++)
        {
            gStar_Anim.transform.GetChild(i).gameObject.SetActive(false);
        }
        if (starFxController.myStarFxController != null)
        {
            starFxController.myStarFxController.ea = 0;
            starFxController.myStarFxController.Reset();
        }
    }
    public void CallNextLevel()
    {
        if (GameUtils.GetFinishTut())
        {

            if (GameUtils.GetLife() > 0)
            {
                gTutorial.SetActive(false);
                if (GameUtils.curLevel <= GameUtils.MaxLevel)
                {
                    GameUtils.isGameRestart = true;
                    CallShowHomeGame();

                    if (GameUtils.isGameRestart)
                    {
                        HexaAnalytics.Instance.EVTutBegin();

                        GameUtils.isGameRestart = false;
                        MenuController.THIS.InitStartGame(GameUtils.curLevel);
                    }
                }
                else
                {
                    ShowNotice(GameUtils.dicLangInUse[GameUtils.MAX_LEVEL]);
                }
            }
            else
            {
                gPanelShop.SetActive(true);
            }
        }
        else
        {
            GameUtils.SetFinishTut();
            HexaAnalytics.Instance.EVTutComplete();
        }
    }
    public void ShowPanelShop() {
        gPanelShop.SetActive(true);
    }
    public void ShowStartPopup() {
        string _textContent = GameUtils.dicLangInUse[GameUtils.TOTAL_STARS_EARNED];
        ShowNotice(_textContent);
    }
    public void ShowNotice(string _notice)
    {
        txtNoti.text = _notice;
        gNoti.SetActive(true);
    }
    IEnumerator PlayLevel(int level)
    {
        MenuController.THIS.gGamePlay.SetActive(false);
        yield return new WaitForSeconds(1.5f);
        MenuController.THIS.gGamePlay.SetActive(false);
    }
    public void CallShowInfo()
    {
        if (gImageInfo.activeSelf)
        {
            gImageInfo.SetActive(false);
        }
        else gImageInfo.SetActive(true);
    }

    public void CloseQuitPanel()
    {
        isQuitGame = false;
        isGamePause = false;
        gPanelMainPlay.SetActive(true);
        if (!MenuController.THIS.gGamePlay.activeSelf)
        {
            MenuController.THIS.scRect.enabled = true;
            MenuController.THIS.gMapLevels.SetActive(true);
        }
        else
        {
            GameUtils.ShowElement(gPanelGrid);
        }
        GameUtils.HideElement(gPanelQuit);
        GameUtils.HideElement(gPanelComplete);
        GameUtils.HideElement(gPanelPause);
    }
    public void CallQuitGame()
    {
        //SceneManager.LoadSceneAsync(GameUtils.SCENE_START);
        SplashScreen.Instance.LoadSene(GameUtils.SCENE_START);
    }
    private void OnApplicationQuit()
    {
        CallShowQuitPanel();
    }
    #region Play Sound when click to Hexa Object
    public IEnumerator DisplayFunction(Transform tr)
    {
        while (_indexNote < tr.childCount)
        {
            tr.GetChild(_indexNote).gameObject.SetActive(true);
            tr.GetChild(_indexNote).gameObject.GetComponent<PianoNote>().PlayNote();
            yield return new WaitForSeconds(timeSwitchNote);
            _indexNote++;
        }
    }
    public int _indexNote;
    #endregion

    #region Capture Screen
    private bool isProcessing = false;
    public RawImage rawImage;
    private void TakeScreenShot()
    {
        if (!isProcessing)
            StartCoroutine(ProcessScreenShot());
    }
    IEnumerator ProcessScreenShot()
    {
        isProcessing = true;
        yield return new WaitForEndOfFrame();
        Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
        screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height), 0, 0);
        screenTexture.Apply();

        rawImage.texture = screenTexture;
        //rawImage.gameObject.SetActive(true);


        #region Share Image to Facebook
        //byte[] dataToSave = screenTexture.EncodeToPNG();
        //string destination = Path.Combine(Application.persistentDataPath, "HexoScreenshot");
        //File.WriteAllBytes(destination, dataToSave);
        //var wwwForm = new WWWForm();
        //wwwForm.AddBinaryData("image", dataToSave, "InteractiveConsole.png");
        //FB.API("me/photos", Facebook.HttpMethod.POST, Callback, wwwForm);
        #endregion

        isProcessing = false;
    }
    #endregion
}