﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class Hexo : MonoBehaviour
{
    public int IdX;
    public int IdY;
    public int CurrentValue;
    public int TargetValue;
    public bool IsLocked;
    public int DependentId;
    public string myKey;
    public List<string> keys;
    public Color clGreen, clCurrent;
    public bool isDirection;
    public int dir = -1;
    public List<MyHintsData> hintsData = new List<MyHintsData>();
    public Sprite sprGroup1, sprGroup2, sprGroup3, sprLock;
    private Animator anim;
    private AudioSource _audio;

    private void ResetData() {
        IdX = 0;
        IdY = 0;
        CurrentValue = 0;
        TargetValue = 0;
        IsLocked = false;
        DependentId = 0;
        myKey = "";
        keys.Clear();
        isDirection = false;
        dir = -1;
        hintsData.Clear();
    }
    private void OnDisable()
    {
        ResetData();
    }
    private void OnEnable()
    {
        clCurrent = GetComponent<Image>().color;
        clGreen = Color.green;
        _audio = GetComponent<AudioSource>();
    }
    // Use this for initialization
    void Start()
    {
        if (GetComponent<Animator>() != null)
        {
            anim = GetComponent<Animator>();
            anim.SetBool("isScale", false);
        }
        if (IsLocked)
        {
            CurrentValue = TargetValue;
            
            MapCreate.Instance.dicAllHexo[myKey].CurrentValue = CurrentValue;
        }
        SetDotActiveByValue(CurrentValue);

        if (!isDirection)
        {
            if(IsLocked) transform.GetChild(1).GetComponent<Image>().sprite = sprLock;
            else transform.GetChild(1).GetComponent<Image>().sprite = MapLoader.Instance.GetHexaSprite().sprDotNormal;
            if (DependentId == 0)
            {
                transform.GetChild(2).GetChild(0).GetComponent<Image>().enabled = false;
            }
            else
            {
                transform.GetChild(2).GetChild(0).GetComponent<Image>().enabled = true;
                switch (DependentId)
                {
                    case 1:
                        transform.GetChild(2).GetChild(0).GetComponent<Image>().sprite = sprGroup1;
                        break;
                    case 2:
                        transform.GetChild(2).GetChild(0).GetComponent<Image>().sprite = sprGroup2;
                        break;
                    case 3:
                        transform.GetChild(2).GetChild(0).GetComponent<Image>().sprite = sprGroup3;
                        break;
                }
            }

            #region Init Dot Color
            //for (int i = 0; i < transform.GetChild(1).childCount; i++)
            //{
            //    for (int j = 0; j < transform.GetChild(1).GetChild(i).childCount; j++)
            //    {
            //        transform.GetChild(1).GetChild(i).GetChild(j).GetComponent<Image>().sprite = MapLoader.Instance.GetHexaSprite().sprDot;
            //    }
            //}
            #endregion
        }

        CheckTutStep();
    }

    private void CheckTutStep()
    {
        if (!GameUtils.GetFinishTut())
        {
                for (int i = 0; i < GameManager.THIS.lstTutStep.Count; i++)
                {
                    if (i == GameUtils.GetTutStep())
                    {
                        GameManager.THIS.lstTutStep[i].GetComponent<Canvas>().overrideSorting = true;
                        GameManager.THIS.gHandTut.SetActive(true);
                        Vector3 vDes = GameManager.THIS.lstTutStep[i].transform.position;
                        GameManager.THIS.gHandTut.transform.position = new Vector3(vDes.x, vDes.y + 0.5f, vDes.z);
                    }
                    else GameManager.THIS.lstTutStep[i].GetComponent<Canvas>().overrideSorting = false;
                }
        }
        else {
            if (GameUtils.isPlayTutorialMap)
            {
                GameManager.THIS.gHandTut.SetActive(true);
                Vector3 vDes = GameManager.THIS.lstTutStep[0].transform.position;
                GameManager.THIS.gHandTut.transform.position = new Vector3(vDes.x, vDes.y + 0.5f, vDes.z);
            }
        }
    }

    public void SetDotActiveByValue(int _CurrentValue)
    {
        if (!isDirection)
        {
            switch (_CurrentValue)
            {
                case 1:
                    GameManager.THIS._indexNote = 0;
                    StartCoroutine(GameManager.THIS.DisplayFunction(GameManager.THIS.gNote1.transform));
                    transform.GetChild(1).GetComponent<Image>().sprite = MapLoader.Instance.GetHexaSprite().sprDot1;
                    break;
                case 2:
                    GameManager.THIS._indexNote = 0;
                    StartCoroutine(GameManager.THIS.DisplayFunction(GameManager.THIS.gNote2.transform));
                    transform.GetChild(1).GetComponent<Image>().sprite = MapLoader.Instance.GetHexaSprite().sprDot1;
                    break;
                case 3:
                    GameManager.THIS._indexNote = 0;
                    StartCoroutine(GameManager.THIS.DisplayFunction(GameManager.THIS.gNote3.transform));
                    transform.GetChild(1).GetComponent<Image>().sprite = MapLoader.Instance.GetHexaSprite().sprDot1;
                    break;
                default:
                    transform.GetChild(1).GetComponent<Image>().sprite = MapLoader.Instance.GetHexaSprite().sprDotNormal;
                    break;
            }

            for (int i = 0; i < transform.GetChild(1).childCount; i++)
            {
                if (i == _CurrentValue - 1)
                {
                    transform.GetChild(1).GetChild(i).gameObject.SetActive(true);
                }
                else
                {
                    transform.GetChild(1).GetChild(i).gameObject.SetActive(false);
                }
            }
        }
    }

    private void OnMouseDown()
    {
        if (IsLocked) return;
        if (MapCreate.Instance.IsWinGame() || GameManager.THIS.isGamePause || GameManager.THIS.isGameOver || GameManager.THIS.gPanelShop.activeSelf) return;

        GameManager.THIS.ShowClickEffect(gameObject);
        PlayAnimScale();

        if (!GameUtils.GetFinishTut() || GameUtils.isPlayTutorialMap)
        {
            #region Not yet finish Game Tutorial
            if (GetComponent<Canvas>().overrideSorting)
            {
                CurrentValue++;
                if (CurrentValue > 3) CurrentValue = 0;

                if (CurrentValue > 0)
                {
                    transform.GetChild(0).gameObject.SetActive(true);
                    transform.GetChild(0).GetComponent<Text>().text = "" + CurrentValue;
                }
                else transform.GetChild(0).gameObject.SetActive(false);
                SetDotActiveByValue(CurrentValue);

                HexoInfo hInfo = new HexoInfo();
                hInfo.IdX = IdX;
                hInfo.IdY = IdY;
                hInfo.IsLocked = IsLocked;
                hInfo.keys.AddRange(keys);
                hInfo.myKey = myKey;
                hInfo.TargetValue = TargetValue;
                hInfo.CurrentValue = CurrentValue;
                hInfo.DependentId = DependentId;
                hInfo.HintsData = hintsData;
                MapCreate.Instance.Search(hInfo);

                GameManager.THIS.gHandTut.SetActive(false);
                for (int i = 0; i < GameManager.THIS.lstTutStep.Count; i++)
                {
                        GameManager.THIS.lstTutStep[i].GetComponent<Canvas>().overrideSorting = true;
                }

                if (myKey == GameManager.THIS.lstTutStep[0].name)
                    {
                        GameManager.THIS.txtParam1.text = CurrentValue + " +";
                    }
                    else if (myKey == GameManager.THIS.lstTutStep[1].name)
                    {
                        GameManager.THIS.txtParam2.text = CurrentValue + " +";
                    }
                    else if (myKey == GameManager.THIS.lstTutStep[2].name) {
                        GameManager.THIS.txtParam3.text = CurrentValue + " +";
                    }
            }
            #endregion
        }
        else if (DependentId != 0)
        {
            #region Same Group/DependentID
            var sameDep = from sHexo in MapCreate.Instance.dicAllHexaObject.Values where (sHexo.GetComponent<Hexo>().DependentId == DependentId) select sHexo;
            foreach (var _sHex in sameDep)
            {
                Hexo _hexeTemp = _sHex.GetComponent<Hexo>();
                HexoInfo hInfo = new HexoInfo();
                hInfo.IdX = _hexeTemp.IdX;
                hInfo.IdY = _hexeTemp.IdY;
                hInfo.IsLocked = _hexeTemp.IsLocked;
                hInfo.keys.AddRange(_hexeTemp.keys);
                hInfo.myKey = _hexeTemp.myKey;
                hInfo.TargetValue = _hexeTemp.TargetValue;
                hInfo.CurrentValue = _hexeTemp.CurrentValue;
                hInfo.DependentId = _hexeTemp.DependentId;
                hInfo.HintsData = _hexeTemp.hintsData;
                _sHex.GetComponent<Hexo>().HandleSameDepOnMouseDown(hInfo);
            }
            #endregion
        }
        else
        {
            #region Other
            ProcessOnMouseDownEvent();
            #endregion
        }
    }

    private void OnMouseUp()
    {
        if (DependentId != 0)
        {
            foreach (HexoInfo hi in MapCreate.Instance.dicAllHexo.Values)
            {
                if (hi.DependentId == DependentId)
                    MapCreate.Instance.Search(hi);
            }
        }
    }

    public void HandleSameDepOnMouseDown(HexoInfo _hInfo)
    {
        CurrentValue++;
        if (CurrentValue > 3) CurrentValue = 0;

        if (CurrentValue > 0)
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(0).GetComponent<Text>().text = "" + CurrentValue;
        }
        else transform.GetChild(0).gameObject.SetActive(false);

        SetDotActiveByValue(CurrentValue);

        HexoInfo hInfo = new HexoInfo();
        hInfo.IdX = IdX;
        hInfo.IdY = IdY;
        hInfo.IsLocked = IsLocked;
        hInfo.keys.AddRange(keys);
        hInfo.myKey = myKey;
        hInfo.TargetValue = TargetValue;
        hInfo.CurrentValue = CurrentValue;
        hInfo.DependentId = DependentId;
        hInfo.HintsData = hintsData;
        MapCreate.Instance.Search(hInfo);

        GameManager.THIS.CheckTurn();

    }
    public void ProcessOnMouseDownEvent()
    {
        CurrentValue++;

        if (CurrentValue > 3) CurrentValue = 0;
        if (CurrentValue > 0)
        {
            transform.GetChild(0).gameObject.SetActive(true);
            transform.GetChild(0).GetComponent<Text>().text = "" + CurrentValue;
        }
        else transform.GetChild(0).gameObject.SetActive(false);

        if (!GameManager.THIS.isUseHint)
        {
            SetDotActiveByValue(CurrentValue);

            HexoInfo hInfo = new HexoInfo();
            hInfo.IdX = IdX;
            hInfo.IdY = IdY;
            hInfo.IsLocked = IsLocked;
            hInfo.keys.AddRange(keys);
            hInfo.myKey = myKey;
            hInfo.TargetValue = TargetValue;
            hInfo.CurrentValue = CurrentValue;
            hInfo.DependentId = DependentId;
            hInfo.HintsData = hintsData;
            MapCreate.Instance.Search(hInfo);

            GameManager.THIS.CheckPlayerMove();


            //GameManager.THIS.curMove++;
            //GameManager.THIS.UpdatePlayerTurn();
            //GameManager.THIS.CheckTurn();
        }
        else
        {
            GameManager.THIS.isUseHint = false;
            GameManager.THIS.CheckUsingHint(gameObject);
        }
    }

    public void PlayAnimScale()
    {
        anim.SetBool("isScale", true);
    }
    public void EndAnimScale()
    {
        anim.SetBool("isScale", false);
    }
}