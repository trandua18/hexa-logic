﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropDownUtils : MonoBehaviour {
    // Use this for initialization
    void Start () {
        transform.localPosition = new Vector3(0, -407, transform.localPosition.z);
        for (int i = 1; i < transform.GetChild(0).GetChild(0).childCount; i++)
        {
            switch (i)
            {
                case 1:
                    transform.GetChild(0).GetChild(0).GetChild(i).GetChild(2).GetComponent<Text>().text = "ENGLISH";
                    break;
                case 2:
                    transform.GetChild(0).GetChild(0).GetChild(i).GetChild(2).GetComponent<Text>().text = "CHINESE";
                    break;
                case 3:
                    transform.GetChild(0).GetChild(0).GetChild(i).GetChild(2).GetComponent<Text>().text = "BRAZIL";
                    break;
                case 4:
                    transform.GetChild(0).GetChild(0).GetChild(i).GetChild(2).GetComponent<Text>().text = "JAPAN";
                    break;
            }
        }
    }
}