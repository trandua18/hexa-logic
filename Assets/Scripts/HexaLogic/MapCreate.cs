﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System.Linq;
using UnityEngine.Rendering;
using DoozyUI;

public class MapCreate : MonoBehaviour
{
    public static MapCreate Instance;

    public Transform trParent;
    public GameObject gDir;
    public GameObject gHexo;
    public int gridWidth = 7;
    public int gridHeight = 10;

    /// <summary>
    /// TODO 
    /// </summary>
    public int _totalHexValue = 0;

    private Dictionary<string, MyClass> myDic;
    public Dictionary<string, GameObject> dicAllHexaObject;
    private Dictionary<string, List<StoreHex>> dicTemp;
    private Dictionary<string, StoreHex> dicSum;
    public Dictionary<string, HexoInfo> dicAllHexo;
    Dictionary<string, int> _dicCount = new Dictionary<string, int>();

    private Dictionary<string, int> dicCount;
    private MyGameLevel myLevels;

    void InitDic()
    {
        myDic = new Dictionary<string, MyClass>();
        dicAllHexaObject = new Dictionary<string, GameObject>();
        dicTemp = new Dictionary<string, List<StoreHex>>();
        dicSum = new Dictionary<string, StoreHex>();
        dicAllHexo = new Dictionary<string, HexoInfo>();
        _dicCount = new Dictionary<string, int>();
        dicCount = new Dictionary<string, int>();
        myLevels = null;
        _totalHexValue = 0;

        if(GameManager.THIS.txtTurn != null)
            GameManager.THIS.txtTurn.GetComponent<Animator>().Play("TextTurnIdle");
    }
    public void ClearCache() {
        myDic = new Dictionary<string, MyClass>();
        dicAllHexaObject = new Dictionary<string, GameObject>();
        dicTemp = new Dictionary<string, List<StoreHex>>();
        dicSum = new Dictionary<string, StoreHex>();
        dicAllHexo = new Dictionary<string, HexoInfo>();
        _dicCount = new Dictionary<string, int>();
        dicCount = new Dictionary<string, int>();
        myLevels = null;
        for (int i = 0; i < trParent.childCount; i++)
        {
            trParent.GetChild(i).gameObject.SetActive(false);
        }
        gameObject.SetActive(false);
    }
    private void OnDisable()
    {
        AdsManager.Instance.HideBanner();
        InitDic();
        if(SoundManager.THIS != null)
            SoundManager.THIS.PlayBackgroundMusic(SoundManager.THIS.acMainPlayBG);
        if (MenuController.THIS != null)
        {
            if(MenuController.THIS.gEffectInMap != null)
                MenuController.THIS.gEffectInMap.SetActive(true);
        }

        //MenuController.THIS.SnapToLevel(GameUtils.GetCurrentLevel());
    }
    private void Awake()
    {
        Instance = this;
        InitDic();

    }
    private void OnEnable()
    {
        LoadLevelResource();
        
        InitGridWithUIImage();
        InitHexagonItem();

        GameManager.THIS.UpdateAllStars();

        SoundManager.THIS.PlaySound(SoundManager.THIS.acNew);
        AdsManager.Instance.ShowBanner();
        MenuController.THIS.gEffectInMap.SetActive(false);
        MenuController.THIS.gRingCurrentMap.SetActive(false);

        SoundManager.THIS._indexBGSound = Random.Range(0, SoundManager.THIS.allInGameSound.Length - 1);
        SoundManager.THIS.PlayBackgroundMusic(/*SoundManager.THIS.acMenuBG*/SoundManager.THIS.allInGameSound[SoundManager.THIS._indexBGSound]);
    }
    // Use this for initialization
    private void InitGridWithUIImage()
    {
        float _offsetX = /*135*/ 135;
        for (int x = 0; x < gridWidth; x++)
        {
            for (int y = 0; y < gridHeight; y++)
            {
                float newX = y % 2 == 0 ? x * _offsetX : x * _offsetX + /*44*/(_offsetX / 2);
                float newY = y * /*120*/115;
                MyClass mClass = new MyClass();
                mClass.posX = newX;
                mClass.posY = newY;
                mClass.idX = x - y / 2 + 9;
                mClass.idY = y + 9;
                string _key = mClass.idX + "~" + mClass.idY;
                myDic.Add(_key, mClass);

                #region Test Full hexa-grids
                //GameObject gHexoNew = Instantiate(gHexo, new Vector3(newX, newY, 0), Quaternion.identity);
                //gHexoNew.GetComponent<Hexo>().IdX = mClass.idX;
                //gHexoNew.GetComponent<Hexo>().IdY = mClass.idY;
                //gHexoNew.GetComponent<Hexo>().isDirection = true;

                //string hexKey = mClass.idX + "-" + mClass.idY;
                //string myKey = gHexoNew.GetComponent<Hexo>().IdX + "-" + gHexoNew.GetComponent<Hexo>().IdY;
                //dicAllHexaObject.Add(myKey, gHexoNew);
                //gHexoNew.transform.SetParent(trParent, false);
                //gHexoNew.SetActive(!isActive(x, y));
                //GameUtils.ShowElement(GameManager.THIS.gPanelGrid);
                #endregion
            }
        }
    }
    private bool isActive(int x, int y)
    {
        return y % 2 != 0 && x == gridWidth - 1;
    }
    void Start()
    {
        SoundManager.THIS.StopBackGroundMusic();
        SoundManager.THIS.PlayBackgroundMusic(/*SoundManager.THIS.acMenuBG*/SoundManager.THIS.allInGameSound[SoundManager.THIS._indexBGSound]);
    }

    private void InitHexagonItem()
    {
        #region Init Direction Object
        for (int i = 0; i < myLevels.HintsContainers.Count; i++)
        {
            MyClass _m = myDic[(myLevels.HintsContainers[i].IdX + "~" + myLevels.HintsContainers[i].IdY)];
            float posX = _m.posX;
            float posY = _m.posY;
            GameObject gHexoNew = Instantiate(gDir, new Vector3(posX, posY, 0), Quaternion.identity);
            gHexoNew.GetComponent<Hexo>().IdX = _m.idX;
            gHexoNew.GetComponent<Hexo>().IdY = _m.idY;
            gHexoNew.GetComponent<Hexo>().isDirection = true;
            if (myDic.ContainsKey(myLevels.HintsContainers[i].IdX + "~" + myLevels.HintsContainers[i].IdY))
            {
                for (int j = 0; j < myLevels.HintsContainers[i].HintsData.Count; j++)
                {
                    MyHintsData mHintData = new MyHintsData();
                    mHintData.Direction = myLevels.HintsContainers[i].HintsData[j].Direction;
                    mHintData.IsActive = myLevels.HintsContainers[i].HintsData[j].IsActive;

                    gHexoNew.GetComponent<Hexo>().hintsData.Add(mHintData);
                    FindByDirection(gHexoNew.GetComponent<Hexo>().IdX, gHexoNew.GetComponent<Hexo>().IdY, mHintData.Direction, gHexoNew.GetComponent<Hexo>().IdX, gHexoNew.GetComponent<Hexo>().IdY);
                    string hexKey = _m.idX + "~" + _m.idY;

                    gHexoNew.transform.GetChild(0).GetChild(mHintData.Direction).gameObject.SetActive(true);
                    gHexoNew.name = hexKey + "~" + mHintData.Direction;
                    gHexoNew.GetComponent<Hexo>().dir = mHintData.Direction;
                    if (dicSum.ContainsKey(hexKey + "~" + mHintData.Direction))
                    {
                        gHexoNew.transform.GetChild(0).GetChild(mHintData.Direction).GetChild(0).GetComponent<Text>().text = "" + dicSum[hexKey + "~" + mHintData.Direction].value;
                    }
                }


                string myKey = gHexoNew.GetComponent<Hexo>().IdX + "~" + gHexoNew.GetComponent<Hexo>().IdY;
                dicAllHexaObject.Add(myKey, gHexoNew);
                gHexoNew.transform.SetParent(trParent, false);
                gHexoNew.SetActive(true);
            }
        }
        #endregion

        #region Init Hexagon Object
        for (int i = 0; i < myLevels.Fields.Count; i++)
        {
            if (myDic.ContainsKey(myLevels.Fields[i].IdX + "~" + myLevels.Fields[i].IdY))
            {
                MyClass _m = myDic[(myLevels.Fields[i].IdX + "~" + myLevels.Fields[i].IdY)];
                float posX = _m.posX;
                float posY = _m.posY;
                GameObject gHexoNew = Instantiate(gHexo, new Vector3(posX, posY, 0), Quaternion.identity);
                gHexoNew.GetComponent<Hexo>().IdX = _m.idX;
                gHexoNew.GetComponent<Hexo>().IdY = _m.idY;
                gHexoNew.GetComponent<Hexo>().CurrentValue = myLevels.Fields[i].CurrentValue;
                gHexoNew.GetComponent<Hexo>().TargetValue = myLevels.Fields[i].TargetValue;
                gHexoNew.GetComponent<Hexo>().IsLocked = myLevels.Fields[i].IsLocked;
                gHexoNew.GetComponent<Hexo>().DependentId = myLevels.Fields[i].DependentId;
                gHexoNew.GetComponent<Hexo>().isDirection = false;
                gHexoNew.GetComponent<Hexo>().myKey = gHexoNew.GetComponent<Hexo>().IdX + "~" + gHexoNew.GetComponent<Hexo>().IdY;
                gHexoNew.GetComponent<Hexo>().keys = new List<string>();
                for (int j = 0; j < myLevels.Fields[i].keys.Count; j++)
                {
                    if (!gHexoNew.GetComponent<Hexo>().keys.Contains(myLevels.Fields[i].keys[j]))
                    {
                        gHexoNew.GetComponent<Hexo>().keys.Add(myLevels.Fields[i].keys[j]);
                    }
                }
                //gHexoNew.GetComponent<Hexo>().keys = myLevels.Fields[i].keys;
                gHexoNew.name = gHexoNew.GetComponent<Hexo>().myKey;
                dicAllHexaObject.Add(gHexoNew.GetComponent<Hexo>().myKey, gHexoNew);
                gHexoNew.transform.SetParent(trParent, false);
                gHexoNew.SetActive(true);


                if (!GameUtils.GetFinishTut() || GameUtils.isPlayTutorialMap)
                {
                    if (!GameManager.THIS.lstTutStep.Contains(gHexoNew))
                        GameManager.THIS.lstTutStep.Add(gHexoNew);
                }
            }
        }
        #endregion

        GameUtils.ShowElement(GameManager.THIS.gPanelGrid);
    }
    #region Find By Direction

    private void FindByDirection(int idX, int idY, int dir, int curIDX, int curIDY)
    {
        int _idY = 0;
        int _idX = 0;
        #region Calculate Dir
        switch (dir)
        {
            case 0:
                _idY = idY + 1;
                _idX = idX;
                break;
            case 1:
                _idY = idY;
                _idX = idX + 1;
                break;
            case 2:
                _idY = idY - 1;
                _idX = idX + 1;
                break;
            case 3:
                _idY = idY - 1;
                _idX = idX;
                break;
            case 4:
                _idY = idY;
                _idX = idX - 1;
                break;
            case 5:
                _idY = idY + 1;
                _idX = idX - 1;
                break;
        }
        #endregion

        string keyHexa = _idX + "~" + _idY;

        if (dicAllHexo.ContainsKey(keyHexa.Trim()))
        {
            StoreHex sHex = new StoreHex();
            sHex.idX = _idX;
            sHex.idY = _idY;
            sHex.value = dicAllHexo[keyHexa].TargetValue;
            string __keyDir = curIDX + "~" + curIDY + "~" + dir;

            if (dicAllHexo[keyHexa].keys != null)
            {
                dicAllHexo[keyHexa].keys.Add(__keyDir);
            }
            else
            {
                dicAllHexo[keyHexa].keys = new List<string>();
                dicAllHexo[keyHexa].keys.Add(__keyDir);
            }

            #region Get Value by Direction
            if (dicSum.ContainsKey(__keyDir))
            {
                dicSum[__keyDir].value += dicAllHexo[keyHexa].TargetValue;
            }
            else
            {
                dicSum.Add(__keyDir.Trim(), sHex);
            }
            #endregion

            #region Add Hexa position by Direction
            if (dicTemp.ContainsKey(__keyDir))
            {
                if (dicTemp[__keyDir] != null)
                {
                    dicTemp[__keyDir].Add(sHex);
                }
            }
            else
            {
                List<StoreHex> lstH_ = new List<StoreHex>();
                lstH_.Add(sHex);
                dicTemp.Add(__keyDir.Trim(), lstH_);
            }
            #endregion

            FindByDirection(_idX, _idY, dir, curIDX, curIDY);
        }
    }
    #endregion


    private void LoadLevelResource()
    {
        //myLevels = MapLoader.Instance.dicAllLevels[/*GameUtils.GetCurrentLevel()*/GameUtils.curLevel];
        //GameManager.THIS.maxMove = myLevels.TurnLimited;
        //for (int i = 0; i < myLevels.Fields.Count; i++)
        //{
        //    HexoInfo hInfo = new HexoInfo();
        //    hInfo.IdX = myLevels.Fields[i].IdX;
        //    hInfo.IdY = myLevels.Fields[i].IdY;
        //    hInfo.CurrentValue = myLevels.Fields[i].CurrentValue;
        //    hInfo.TargetValue = myLevels.Fields[i].TargetValue;
        //    hInfo.IsLocked = myLevels.Fields[i].IsLocked;
        //    hInfo.DependentId = myLevels.Fields[i].DependentId;
        //    hInfo.myKey = hInfo.IdX + "~" + hInfo.IdY;
        //    hInfo.keys = new List<string>();
        //    hInfo.keys = myLevels.Fields[i].keys;

        //    string key = hInfo.IdX + "~" + hInfo.IdY;
        //    dicAllHexo.Add(key.Trim(), hInfo);
        //}

        string levelToLoad = GameUtils.curLevel + "";
        if (!GameUtils.GetFinishTut())
        {
            levelToLoad = GameUtils.levelTutorial;
        }
        else if (GameUtils.TEST) { levelToLoad = "Tutorial/MyFile"; }
        else
        {
            if(GameUtils.isPlayTutorialMap) levelToLoad = GameUtils.levelTutorial;
            else 
                levelToLoad = "Levels/" + GameUtils.curLevel;
        }

        int totalValue = 0;
        TextAsset ta = Resources.Load<TextAsset>(levelToLoad);
        myLevels = new MyGameLevel();

        JSONNode jLevels = JSON.Parse(ta.text);
        
        List<HexoInfo> Fields = new List<HexoInfo>();
        for (int i = 0; i < jLevels["Fields"].Count; i++)
        {
            HexoInfo hInfo = new HexoInfo();
            hInfo.IdX = jLevels["Fields"][i]["IdX"];
            hInfo.IdY = jLevels["Fields"][i]["IdY"];
            hInfo.CurrentValue = jLevels["Fields"][i]["CurrentValue"];
            hInfo.TargetValue = jLevels["Fields"][i]["TargetValue"];
            hInfo.IsLocked = jLevels["Fields"][i]["IsLocked"];
            hInfo.DependentId = jLevels["Fields"][i]["DependentId"];
            hInfo.myKey = hInfo.IdX + "~" + hInfo.IdY;
            Fields.Add(hInfo);

            totalValue += hInfo.TargetValue;
            string key = hInfo.IdX + "~" + hInfo.IdY;
            dicAllHexo.Add(key.Trim(), hInfo);
        }
        myLevels.TurnLimited = totalValue + (int)(totalValue * 0.5f);
        GameManager.THIS.maxMove = myLevels.TurnLimited;
        #region Turn Extra
        int turnExtra = 0;
        if (GameUtils.HasBuyExtraTurn())
        {
            turnExtra = GameManager.THIS.maxMove + (int)(GameManager.THIS.maxMove * 0.5f);
        }
        #endregion

        GameManager.THIS.maxMove += turnExtra;
        GameManager.THIS.curMove = 0;
        _totalHexValue = totalValue + turnExtra;
        GameManager.THIS.UpdatePlayerTurn();

        myLevels.Fields = Fields;
        List<MyHintsContainers> HintsContainers = new List<MyHintsContainers>();

        for (int j = 0; j < jLevels["HintsContainers"].Count; j++)
        {
            List<MyHintsData> HintsData = new List<MyHintsData>();
            MyHintsContainers mHintCotainer = new MyHintsContainers();
            mHintCotainer.IdX = jLevels["HintsContainers"][j]["IdX"];
            mHintCotainer.IdY = jLevels["HintsContainers"][j]["IdY"];
            for (int k = 0; k < jLevels["HintsContainers"][j]["HintsData"].Count; k++)
            {
                MyHintsData mHintData = new MyHintsData();
                mHintData.Direction = jLevels["HintsContainers"][j]["HintsData"][k]["Direction"];
                mHintData.IsActive = jLevels["HintsContainers"][j]["HintsData"][k]["IsActive"];
                HintsData.Add(mHintData);
            }
            mHintCotainer.HintsData = HintsData;
            HintsContainers.Add(mHintCotainer);
        }
        myLevels.HintsContainers = HintsContainers;
    }



    private bool CheckZero(HexoInfo hInfo)
    {
        bool bTemp = false;
        int sum = 0;
        for (int i = 0; i < hInfo.keys.Count; i++)
        {
            for (int j = 0; j < dicTemp[hInfo.keys[i]].Count; j++)
            {
                string _key = dicTemp[hInfo.keys[i]][j].idX + "~" + dicTemp[hInfo.keys[i]][j].idY;
                sum += dicAllHexo[_key].CurrentValue;
                if (dicAllHexo[_key].CurrentValue != 0) bTemp = true;
                else bTemp = false;
            }
        }
        return bTemp;
    }

    public void Search(HexoInfo hInfo)
    {
        ///TODO Check current Value with target value
        dicAllHexo[hInfo.myKey].CurrentValue = hInfo.CurrentValue;

        dicCount = new Dictionary<string, int>();
        for (int i = 0; i < hInfo.keys.Count; i++)
        {
            for (int j = 0; j < dicTemp[hInfo.keys[i]].Count; j++)
            {
                string _key = dicTemp[hInfo.keys[i]][j].idX + "~" + dicTemp[hInfo.keys[i]][j].idY;
                if (dicCount.ContainsKey(hInfo.keys[i]))
                {
                    dicCount[hInfo.keys[i]] += dicAllHexo[_key].CurrentValue;
                }
                else
                {
                    dicCount[hInfo.keys[i]] = dicAllHexo[_key].CurrentValue;
                }
            }
        }


        for (int i = 0; i < hInfo.keys.Count; i++)
        {
            string _keyDicTemp = hInfo.keys[i].Substring(0, hInfo.keys[i].Length - 2);
            int _dir = int.Parse(hInfo.keys[i].Substring(hInfo.keys[i].Length - 1));
            for (int j = 0; j < dicTemp[hInfo.keys[i]].Count; j++)
            {
                string _keyHex = dicTemp[hInfo.keys[i]][j].idX + "~" + dicTemp[hInfo.keys[i]][j].idY;
                bool isTrue = dicAllHexo.Values.Any(x => x.CurrentValue == 0 && x.keys.Contains(hInfo.keys[i]));//Check currentValue in dicAllHexo
                if (!isTrue)
                {
                    if (dicCount[hInfo.keys[i]] == dicSum[hInfo.keys[i]].value)
                    {
                        #region Set Color for Direction
                        dicAllHexaObject[_keyDicTemp].transform.GetChild(0).GetChild(_dir).GetComponent<Image>().color = dicAllHexaObject[_keyDicTemp].GetComponent<Hexo>().clGreen;
                        dicAllHexaObject[_keyDicTemp].transform.GetChild(0).GetChild(_dir).GetChild(0).GetComponent<Text>().color = Color.white;
                        #endregion
                        dicSum[hInfo.keys[i]].isValid = true;
                        SoundManager.THIS.PlaySound(SoundManager.THIS.acFinish);
                    }
                    else
                    {
                        dicAllHexaObject[_keyHex].GetComponent<Image>().color = dicAllHexaObject[_keyHex].GetComponent<Hexo>().clCurrent;
                        dicAllHexaObject[_keyHex].GetComponent<Canvas>().sortingLayerName = "HexaLayer";
                        dicAllHexaObject[_keyHex].GetComponent<Canvas>().sortingOrder = 2;
                        #region Set Color for Direction
                        dicAllHexaObject[_keyDicTemp].transform.GetChild(0).GetChild(_dir).GetComponent<Image>().color = dicAllHexaObject[_keyDicTemp].GetComponent<Hexo>().clCurrent;
                        dicAllHexaObject[_keyDicTemp].transform.GetChild(0).GetChild(_dir).GetChild(0).GetComponent<Text>().color = Color.black;
                        #endregion

                        dicSum[hInfo.keys[i]].isValid = false;
                    }
                }
                else
                {

                    dicAllHexaObject[_keyHex].GetComponent<Image>().color = dicAllHexaObject[_keyHex].GetComponent<Hexo>().clCurrent;
                    #region Set Color for Direction
                    dicAllHexaObject[_keyDicTemp].transform.GetChild(0).GetChild(_dir).GetComponent<Image>().color = dicAllHexaObject[_keyDicTemp].GetComponent<Hexo>().clCurrent;
                    dicAllHexaObject[_keyDicTemp].transform.GetChild(0).GetChild(_dir).GetChild(0).GetComponent<Text>().color = Color.black;
                    #endregion
                    dicSum[hInfo.keys[i]].isValid = false;
                }
            }
        }

        var mm1 = from sHex_ in dicSum where (sHex_.Value.isValid == true) select sHex_;
        foreach (var _myShex in mm1)
        {
            foreach (HexoInfo hi in dicAllHexo.Values)
            {
                if (hi.keys.Contains(_myShex.Key))
                {
                    dicAllHexaObject[hi.myKey].GetComponent<Image>().color = dicAllHexaObject[hi.myKey].GetComponent<Hexo>().clGreen;
                    dicAllHexaObject[hi.myKey].GetComponent<Canvas>().sortingLayerName = "HexaLayer";
                    dicAllHexaObject[hi.myKey].GetComponent<Canvas>().sortingOrder = 3;
                }
            }
        }

        if (IsWinGame() && (GameManager.THIS.curMove < GameManager.THIS.maxMove))
        {
            SoundManager.THIS.PlaySound(SoundManager.THIS.acWin);
            StartCoroutine(WaitForShowCompleteUI());
        }
    }

    IEnumerator WaitForShowCompleteUI() {
        yield return new WaitForSeconds(1.5f);
        GameManager.THIS.ShowLevelComplete();
        if (GameUtils.GetFinishTut())
        {
            GameManager.THIS.CalculateStar();
            GameUtils.SetTotalStars(GameManager.THIS.CalculateStar());
            if (GameUtils.curLevel <= GameUtils.MaxLevel)
            {
                GameUtils.curLevel += 1;
                LevelDots.THIS.InitLevelSelected();
                GameUtils.NextLevel();
            }
        }
        else
        {
            GameUtils.curLevel = 1;
            GameUtils.SetFinishTut();
            HexaAnalytics.Instance.EVTutComplete();
        }
    }
    public bool IsWinGame()
    {
        string[] _sumKey = dicSum.Keys.ToArray();
        for (int i = 0; i < _sumKey.Length; i++)
        {
            if (!dicSum[_sumKey[i]].isValid) return false;
        }
        return true;
    }
}

public class MyGameLevel
{
    public string LevelID { get; set; }
    public List<HexoInfo> Fields;
    public List<MyHintsContainers> HintsContainers { get; set; }
    public string Title { get; set; }
    public int TurnLimited { get; set; }
    public bool IsProgressive { get; set; }
}
public class HexoInfo
{
    public int IdX { get; set; }
    public int IdY { get; set; }
    public int CurrentValue { get; set; }
    public int TargetValue { get; set; }
    public bool IsLocked { get; set; }
    public int DependentId { get; set; }

    public string myKey { get; set; }
    public List<string> keys = new List<string>();
    public List<MyHintsData> HintsData { get; set; }
}
public class MyHintsContainers
{
    public int IdX { get; set; }
    public int IdY { get; set; }
    public List<MyHintsData> HintsData { get; set; }
}
public class MyHintsData
{
    public int Direction { get; set; }
    public bool IsActive { get; set; }
}
///MyClass- using for put position and ID
public class MyClass
{
    public float posX { get; set; }
    public float posY { get; set; }
    public int idX { get; set; }
    public int idY { get; set; }
}


///------
public class StoreHex
{
    public int idX { get; set; }
    public int idY { get; set; }
    public int dir { get; set; }
    public int value { get; set; }
    public string strS { get; set; }
    public bool isValid { get; set; }
}