﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class MapLoader : MonoBehaviour {
    public List<HexaSprite> lstHexaSprite;
    public static MapLoader Instance;

    public Dictionary<int, MyGameLevel> dicAllLevels = new Dictionary<int, MyGameLevel>();

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
            InitMapLevels();
        }
        else Destroy(gameObject);
    }
    public HexaSprite GetHexaSprite() {
        HexaSprite _hexSprite = new HexaSprite();
        int _curLevel = GameUtils.curLevel;
        if (_curLevel > 0 && _curLevel <= 10) _hexSprite = lstHexaSprite[0];
        else if (_curLevel > 10 && _curLevel <= 20) _hexSprite = lstHexaSprite[1];
        else if (_curLevel > 20 && _curLevel <= 30) _hexSprite = lstHexaSprite[2];
        else if (_curLevel > 30 && _curLevel <= 40) _hexSprite = lstHexaSprite[3];
        else if (_curLevel > 40 && _curLevel <= 50) _hexSprite = lstHexaSprite[4];
        else _hexSprite = lstHexaSprite[0];
        return _hexSprite;
    }

    private void InitMapLevels() {
        TextAsset[] ta = Resources.LoadAll<TextAsset>("Levels");
        for (int tai = 0; tai < ta.Length; tai++) {
            MyGameLevel myLevels = new MyGameLevel();

            JSONNode jLevels = JSON.Parse(ta[tai].text);
            myLevels.TurnLimited = jLevels["TurnLimited"];
            List<HexoInfo> Fields = new List<HexoInfo>();

            for (int i = 0; i < jLevels["Fields"].Count; i++)
            {
                HexoInfo hInfo = new HexoInfo();
                hInfo.IdX = jLevels["Fields"][i]["IdX"];
                hInfo.IdY = jLevels["Fields"][i]["IdY"];
                hInfo.CurrentValue = jLevels["Fields"][i]["CurrentValue"];
                hInfo.TargetValue = jLevels["Fields"][i]["TargetValue"];
                hInfo.IsLocked = jLevels["Fields"][i]["IsLocked"];
                hInfo.DependentId = jLevels["Fields"][i]["DependentId"];
                hInfo.myKey = hInfo.IdX + "~" + hInfo.IdY;
                Fields.Add(hInfo);

                string key = hInfo.IdX + "~" + hInfo.IdY;
            }
            myLevels.Fields = Fields;
            List<MyHintsContainers> HintsContainers = new List<MyHintsContainers>();

            for (int j = 0; j < jLevels["HintsContainers"].Count; j++)
            {
                List<MyHintsData> HintsData = new List<MyHintsData>();
                MyHintsContainers mHintCotainer = new MyHintsContainers();
                mHintCotainer.IdX = jLevels["HintsContainers"][j]["IdX"];
                mHintCotainer.IdY = jLevels["HintsContainers"][j]["IdY"];
                for (int k = 0; k < jLevels["HintsContainers"][j]["HintsData"].Count; k++)
                {
                    MyHintsData mHintData = new MyHintsData();
                    mHintData.Direction = jLevels["HintsContainers"][j]["HintsData"][k]["Direction"];
                    mHintData.IsActive = jLevels["HintsContainers"][j]["HintsData"][k]["IsActive"];
                    HintsData.Add(mHintData);
                }
                mHintCotainer.HintsData = HintsData;
                HintsContainers.Add(mHintCotainer);
            }
            myLevels.HintsContainers = HintsContainers;


            dicAllLevels.Add(tai + 1, myLevels);
        }
        GameUtils.MaxLevel =  ta.Length;
    }
}