﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SimpleJSON;
using System.Linq;
using DoozyUI;
using DG.Tweening;
using System;

public class GameUtils{
    public enum ITEM_TYPE { REMOVE_ADS, BUY_HINTS, EXTRA_TURN, BUY_MORE_LIFE }

    public static bool GAME_DEBUG = false;
    public const bool TEST = false;
    public const string ID_PUBLISHER = "ca-app-pub-8324630314214035~8386139833";
    public const string ID_BANNER = "ca-app-pub-8324630314214035/2546941050";
    public const string ID_REWARDED_VIDEO = "ca-app-pub-8324630314214035/3476879342";
    public const string ID_INTERSTITIAL = "ca-app-pub-8324630314214035/4350977310";
    public const string SCENE_MAIN_MENU = "MainMenu";
    public const string SCENE_MAIN = "MainScene";
    public const string SCENE_START = "StartScene";
    public const string GAME_PACK = "com.sudoku.hexa.puzzle";
    const string GAME_KEY = "HEXA-LOGIC-";
    public const string TOTAL_STARS = GAME_KEY + "TOTAL-STARS";
    public const string TOTAL_HINTS = GAME_KEY + "TOTAL-HINTS";
    public const string SOUND_ON = GAME_KEY + "SOUND-ON";
    public const string GOOGLE_LOGIN = GAME_KEY + "GOOGLE-LOGIN";
    public const string GAME_NO_ADS = GAME_KEY + "GAME-NO-ADS";
    public const string GAME_TUTORIAL = GAME_KEY + "GAME-TUTORIAL";
    public const string CURRENT_MAP = GAME_KEY + "CURRENT-MAP";
    public const string KEY_LANGUAGE_IN_USE = GAME_KEY + "KEY_LANGUAGE_IN_USE";
    public const string TUT_STEP = GAME_KEY + "TUT_GAME_STEP";
    public const string CHAP_UNLOCK = GAME_KEY + "CHAP_UNLOCK";
    public const string CHAP_SELECTED = GAME_KEY + "CHAP_SELECTED";
    public const string HAS_BUY_EXTRA_TURN = GAME_KEY + "-HAS-BUY-EXTRA-TURN";
    public static int MaxLevel = 0;
    public static bool isGameRestart = false;

    public static string levelTutorial = "Tutorial/tut";
    public static int totalLevelComplete = 0;

    public static Dictionary<string, string> dicLangInUse, dicJAP, dicUSA, dicCN, dicBRZ;
    public static List<Dictionary<string, string>> lstDic = new List<Dictionary<string, string>>();
    #region Language KEY
    public const string COMPLETE_TUT = "COMPLETE_TUT";
    public const string NOT_E_HINTS = "NOT_E_HINTS";
    public const string STARS = "STARS";
    public const string MAX_LEVEL = "MAX_LEVEL";
    public const string CONGRA = "CONGRA";
    public const string LEVEL_COMMING = "LEVEL_COMMING";
    public const string BUY_HINT_TITLE = "BUY_HINT";
    public const string BUY_10_HINTS = "BUY_10_HINTS";
    public const string BUY_MORE_HINT = "BUY_MORE_HINT";
    public const string RESTORE_TITLE = "RESTORE_TITLE";
    public const string ARE_YOU_SURE = "ARE_YOU_SURE";
    public const string QUIT_GAME = "QUIT_GAME";
    public const string SETTING_TEXT = "SETTING_TEXT";
    public const string RATE_US_TEXT = "RATE_US";
    public const string THANKS_4_RATE = "THANKS_4_RATE";
    public const string INFO_KEY = "INFORMATION_KEY";
    public const string SUCCESSFUL_KEY = "SUCCESSFUL_KEY";
    public const string SHOP_TITLE_KEY = GAME_KEY + "-SHOP-TITLE";
    public const string REMOVE_ADS_KEY = GAME_KEY + "-REMOVEADS-TITLE";
    public const string REMOVE_ADS_CONTENT = GAME_KEY + "-REMOVEADS-CONTENT";
    public const string BUY_HINT_CONTENT = GAME_KEY + "-BUYHINT-CONTENT";
    public const string BUY_EXTRA_TURN_TITLE = GAME_KEY + "-BUY-EXTRA-TITLE";
    public const string BUY_EXTRA_TURN_CONTENT = GAME_KEY + "-BUY-EXTRA-CONTENT";
    public const string BUY_LIFE_TITLE = GAME_KEY + "-BUY-LIFE-TITLE";
    public const string BUY_LIFE_CONTENT = GAME_KEY + "-BUY-LIFE-CONTENT";
    public const string CONFIRM_TITLE = GAME_KEY + "-CONFIRM_TITLE";
    public const string CONFIRM_CONTENT = GAME_KEY + "-CONFIRM_CONTENT";
    public const string TRY_AGAIN_TEXT = "TRY-AGAIN-TEXT";
    public const string TOTAL_STARS_EARNED = "TOTAL-STARS-EARNED";
    public const string QUIT_CONTENT = "QUIT-GAME-CONTENT";

    #endregion

    #region Language
    public static void SetCurLanguage(int _index) {
        PlayerPrefs.SetInt(KEY_LANGUAGE_IN_USE, _index);
        PlayerPrefs.Save();
    }
    public static int GetCurLanguage() {
        return PlayerPrefs.GetInt(KEY_LANGUAGE_IN_USE, 0);
    }
    public static void InitLanguage() {
        dicBRZ = new Dictionary<string, string>();
        dicCN = new Dictionary<string, string>();
        dicJAP = new Dictionary<string, string>();
        dicUSA = new Dictionary<string, string>();
        dicLangInUse = new Dictionary<string, string>();

        #region USA
        dicUSA.Add(COMPLETE_TUT, "You had complete tutorial mode.");
        dicUSA.Add(NOT_E_HINTS, "Not Enough Hints!");
        dicUSA.Add(STARS, " Stars");
        dicUSA.Add(MAX_LEVEL, "You reach max level!");
        dicUSA.Add(CONGRA, "Congratulation");
        dicUSA.Add(LEVEL_COMMING, "Level will coming soon!");
        dicUSA.Add(BUY_HINT_TITLE, "BUY HINT");
        dicUSA.Add(BUY_10_HINTS, "Buy 10 hints");
        dicUSA.Add(RESTORE_TITLE, "RESTORE");
        dicUSA.Add(ARE_YOU_SURE, "Are you sure?");
        dicUSA.Add(QUIT_GAME, "QUIT GAME");
        dicUSA.Add(SETTING_TEXT, "SETTING");
        dicUSA.Add(RATE_US_TEXT, "");
        dicUSA.Add(THANKS_4_RATE, "Thanks for rating for us.");
        dicUSA.Add(BUY_MORE_HINT, "Buy more hints?");
        dicUSA.Add(INFO_KEY, "Info");
        dicUSA.Add(SUCCESSFUL_KEY, "SUCESSFUL");
        dicUSA.Add(SHOP_TITLE_KEY, "SHOP");
        dicUSA.Add(REMOVE_ADS_KEY, "REMOVE ADS");
        dicUSA.Add(REMOVE_ADS_CONTENT, "Purchase this to remove all ads in game");
        dicUSA.Add(BUY_HINT_CONTENT, "Get 10 hints");
        dicUSA.Add(BUY_EXTRA_TURN_TITLE, "EXTRA TURN");
        dicUSA.Add(BUY_EXTRA_TURN_CONTENT, "Max turn bonus 20% (pernament)");
        dicUSA.Add(BUY_LIFE_TITLE, "LIFE");
        dicUSA.Add(BUY_LIFE_CONTENT, "Recharge life to full");
        dicUSA.Add(CONFIRM_TITLE, "CONFIRM");
        dicUSA.Add(CONFIRM_CONTENT, "Do you want purchase this item?");
        dicUSA.Add(TRY_AGAIN_TEXT, "TRY AGAIN");
        dicUSA.Add(TOTAL_STARS_EARNED, "Total stars earned.");


        dicUSA.Add(QUIT_CONTENT, "Do you want to exits game?");
        #endregion

        #region BRAZIN
        dicBRZ.Add(COMPLETE_TUT, "Você teve o modo tutorial completo.");
        dicBRZ.Add(NOT_E_HINTS, "Não é suficiente dicas!");
        dicBRZ.Add(STARS, " Estrelas");
        dicBRZ.Add(MAX_LEVEL, "Você atinge o nível máximo!");
        dicBRZ.Add(CONGRA, "Congratulação");
        dicBRZ.Add(LEVEL_COMMING, "Nível virá em breve!");
        dicBRZ.Add(BUY_HINT_TITLE, "COMPRE A SUGESTÃO");
        dicBRZ.Add(BUY_10_HINTS, "Compre 10 dicas");
        dicBRZ.Add(RESTORE_TITLE, "RESTAURAR");
        dicBRZ.Add(ARE_YOU_SURE, "Você tem certeza?");
        dicBRZ.Add(QUIT_GAME, "DESISTIR DO JOGO");
        dicBRZ.Add(SETTING_TEXT, "INSTALAÇÃO");
        dicBRZ.Add(RATE_US_TEXT, "");
        dicBRZ.Add(THANKS_4_RATE, "Obrigado pela classificação para nós.");
        dicBRZ.Add(BUY_MORE_HINT, "Compre mais dicas?");
        dicBRZ.Add(INFO_KEY, "informação");
        dicBRZ.Add(SUCCESSFUL_KEY, "bem sucedido");
        dicBRZ.Add(SHOP_TITLE_KEY, "FAZER COMPRAS");
        dicBRZ.Add(REMOVE_ADS_KEY, "REMOVER PROPAGANDAS");
        dicBRZ.Add(REMOVE_ADS_CONTENT, "Compre isto para remover todos os anúncios no jogo");
        dicBRZ.Add(BUY_HINT_CONTENT, "Receba 10 dicas");
        dicBRZ.Add(BUY_EXTRA_TURN_TITLE, "VOLTA EXTRA");
        dicBRZ.Add(BUY_EXTRA_TURN_CONTENT, "Bônus de giro máximo de 20% (permanente)");
        dicBRZ.Add(BUY_LIFE_TITLE, "VIDA");
        dicBRZ.Add(BUY_LIFE_CONTENT, "Recarregue a vida para o máximo");
        dicBRZ.Add(CONFIRM_TITLE, "CONFIRME");
        dicBRZ.Add(CONFIRM_CONTENT, "Você quer comprar este item?");
        dicBRZ.Add(TRY_AGAIN_TEXT, "TENTE NOVAMENTE");
        dicBRZ.Add(TOTAL_STARS_EARNED, "Total de estrelas ganhas.");

        dicBRZ.Add(QUIT_CONTENT, "Você quer sair do jogo?");
        #endregion

        #region CHINA
        dicCN.Add(COMPLETE_TUT, "你有完整的教程模式。");
        dicCN.Add(NOT_E_HINTS, "没有足够的提示！");
        dicCN.Add(STARS, " 明星");
        dicCN.Add(MAX_LEVEL, "你达到最高水平！");
        dicCN.Add(CONGRA, "祝贺");
        dicCN.Add(LEVEL_COMMING, "等级即将推出！");
        dicCN.Add(BUY_HINT_TITLE, "买东西");
        dicCN.Add(BUY_10_HINTS, "购买10个提示");
        dicCN.Add(RESTORE_TITLE, "恢复");
        dicCN.Add(ARE_YOU_SURE, "你确定？");
        dicCN.Add(QUIT_GAME, "退出游戏");
        dicCN.Add(SETTING_TEXT, "设置");
        dicCN.Add(RATE_US_TEXT, "");
        dicCN.Add(THANKS_4_RATE, "感谢您对我们的评价。");
        dicCN.Add(BUY_MORE_HINT, "购买更多提示？");
        dicCN.Add(INFO_KEY, "信息");
        dicCN.Add(SUCCESSFUL_KEY, "成功");
        dicCN.Add(SHOP_TITLE_KEY, "店");
        dicCN.Add(REMOVE_ADS_KEY, "移除广告");
        dicCN.Add(REMOVE_ADS_CONTENT, "购买此商品即可删除游戏中的所有广告");
        dicCN.Add(BUY_HINT_CONTENT, "得到10个提示");
        dicCN.Add(BUY_EXTRA_TURN_TITLE, "额外的转");
        dicCN.Add(BUY_EXTRA_TURN_CONTENT, "最高转奖金20％（永久）");
        dicCN.Add(BUY_LIFE_TITLE, "生活");
        dicCN.Add(BUY_LIFE_CONTENT, "将生命充实");
        dicCN.Add(CONFIRM_TITLE, "确认");
        dicCN.Add(CONFIRM_CONTENT, "你想要这个商品吗？");
        dicCN.Add(TRY_AGAIN_TEXT, "再试一次");
        dicCN.Add(TOTAL_STARS_EARNED, "获得的总星数。");

        dicCN.Add(QUIT_CONTENT, "你想退出游戏吗？");
        #endregion

        #region JAPAN
        dicJAP.Add(COMPLETE_TUT, "あなたは完全なチュートリアルモードを持っていました。");
        dicJAP.Add(NOT_E_HINTS, "不十分なヒント！");
        dicJAP.Add(STARS, " 星");
        dicJAP.Add(MAX_LEVEL, "あなたは最大レベルに達する！");
        dicJAP.Add(CONGRA, "おめでとう");
        dicJAP.Add(LEVEL_COMMING, "すぐにレベルが来ます！");
        dicJAP.Add(BUY_HINT_TITLE, "ヒントを購入する");
        dicJAP.Add(BUY_10_HINTS, "10ヒントを購入する");
        dicJAP.Add(RESTORE_TITLE, "リストア");
        dicJAP.Add(ARE_YOU_SURE, "本気ですか？");
        dicJAP.Add(QUIT_GAME, "ゲームをやめる");
        dicJAP.Add(SETTING_TEXT, "セットアップ");
        dicJAP.Add(RATE_US_TEXT, "");
        dicJAP.Add(THANKS_4_RATE, "私たちの評価に感謝します。");
        dicJAP.Add(BUY_MORE_HINT, "もっとヒントを購入する？");
        dicJAP.Add(INFO_KEY, "情報");
        dicJAP.Add(SUCCESSFUL_KEY, "成功した");
        dicJAP.Add(SHOP_TITLE_KEY, "ショップ");
        dicJAP.Add(REMOVE_ADS_KEY, "広告を削除");
        dicJAP.Add(REMOVE_ADS_CONTENT, "これを購入してゲーム内のすべての広告を削除する");
        dicJAP.Add(BUY_HINT_CONTENT, "10ヒントを得る");
        dicJAP.Add(BUY_EXTRA_TURN_TITLE, "余分なターン");
        dicJAP.Add(BUY_EXTRA_TURN_CONTENT, "最大ターンボーナス20％（永久）");
        dicJAP.Add(BUY_LIFE_TITLE, "生活");
        dicJAP.Add(BUY_LIFE_CONTENT, "人生を満喫する");
        dicJAP.Add(CONFIRM_TITLE, "確認");
        dicJAP.Add(CONFIRM_CONTENT, "この商品を購入しますか？");
        dicJAP.Add(TRY_AGAIN_TEXT, "再試行する");
        dicJAP.Add(TOTAL_STARS_EARNED, "合計得点");

        dicJAP.Add(QUIT_CONTENT, "あなたはゲームを終了したいですか？");
        #endregion
    }
    #endregion

    #region CHAPTER
    public static void SetUnlockChapter(int chapIndex) {
        PlayerPrefs.SetInt(CHAP_UNLOCK, chapIndex);
        PlayerPrefs.Save();
    }
    public static int GetChapUnlock() {
        return PlayerPrefs.GetInt(CHAP_UNLOCK, 0);
    }
    public static void SelectChapter(int chapIndex) {
        PlayerPrefs.SetInt(CHAP_SELECTED, chapIndex);
        PlayerPrefs.Save();
    }
    public static int GetChapSelected() {
        return PlayerPrefs.GetInt(CHAP_SELECTED, 0);
    }
    #endregion

    #region Get Current Map Index
    public static int curLevel = 0;
    public static void SetLevel(int _level) {
        PlayerPrefs.SetInt(CURRENT_MAP, _level);
        PlayerPrefs.Save();
    }
    public static void NextLevel() {
        //Debug.LogError(curLevel + " vs " + GetCurrentLevel());
        if (curLevel >= GetCurrentLevel())
        {
            int _curMap = PlayerPrefs.GetInt(CURRENT_MAP, 1);
            PlayerPrefs.SetInt(CURRENT_MAP, _curMap + 1);
            PlayerPrefs.Save();
        }
    }
    public static int GetCurrentLevel() {
        return PlayerPrefs.GetInt(CURRENT_MAP, 1);
    }
    #endregion

    #region Game Tutorial
    public static bool isPlayTutorialMap = false;
    public static void SetFinishTut()
    {
        PlayerPrefs.SetInt(GAME_TUTORIAL, 1);
    }
    public static bool GetFinishTut() {
        return PlayerPrefs.GetInt(GAME_TUTORIAL, 0) == 1 ? true : false;
    }
    public static void SetTutStep() {
        int curStep = GetTutStep();
        PlayerPrefs.SetInt(TUT_STEP, curStep + 1);
        PlayerPrefs.Save();
    }
    public static void ResetTutStep() {
        PlayerPrefs.SetInt(TUT_STEP, 0);
        PlayerPrefs.Save();
    }
    public static int GetTutStep() {
        return PlayerPrefs.GetInt(TUT_STEP, 0);
    }
    #endregion

    #region Game No Ads
    public static void SetRemoveAds() {
        PlayerPrefs.SetInt(GAME_NO_ADS, 1);
    }
    public static bool GetNoAds() {
        return PlayerPrefs.GetInt(GAME_NO_ADS, 0) == 1 ? true : false;
    }
    #endregion

    #region Sound
    public static void SetSound() {
        int _cur = PlayerPrefs.GetInt(SOUND_ON, 1);
        int _newState = _cur == 0 ? 1 : 0;
        PlayerPrefs.SetInt(SOUND_ON, _newState);
    }
    public static bool GetSound() {
        return PlayerPrefs.GetInt(SOUND_ON, 1) == 0 ? false : true;
    }
    #endregion

    #region Google Login
    public static void SetGGLogin()
    {
        int _cur = PlayerPrefs.GetInt(GOOGLE_LOGIN, 0);
        int _newState = _cur == 0 ? 1 : 0;
        PlayerPrefs.SetInt(GOOGLE_LOGIN, _newState);
    }
    public static bool GetGGLogin()
    {
        return PlayerPrefs.GetInt(GOOGLE_LOGIN, 0) == 0 ? false : true;
    }
    #endregion

    #region TOTAL STARS
    public static void SetTotalStars(int starsBonus) {
        int curStars = GetTotalStars();
        PlayerPrefs.SetInt(TOTAL_STARS, curStars + starsBonus);
        PlayerPrefs.Save();
    }
    public static int GetTotalStars() {
        return PlayerPrefs.GetInt(TOTAL_STARS, 0);
    }
    #endregion


    #region Player has buy extra turn
    public static void SetBuyExtraTurn() {
        PlayerPrefs.SetInt(HAS_BUY_EXTRA_TURN, 1);
        PlayerPrefs.Save();
    }
    public static bool HasBuyExtraTurn() {
        return PlayerPrefs.GetInt(HAS_BUY_EXTRA_TURN, 0) == 1 ? true : false;
    }
    #endregion

    #region TOTAL HINTS
    public static void SetTotalHints(int hintsBonus)
    {
        int curHints = GetTotalHints();
        PlayerPrefs.SetInt(TOTAL_HINTS, curHints + hintsBonus);
        PlayerPrefs.Save();
    }
    public static int GetTotalHints()
    {
        return PlayerPrefs.GetInt(TOTAL_HINTS, 0);
    }
    #endregion

    #region Show-Hide Doozy Element
    public static void ShowElement(/*UIElement*/GameObject _uie)
    {
        _uie.GetComponent<UIElement>().Show(false);
    }
    public static void HideElement(/*UIElement*/GameObject _uie) {
        UIManager.HideUiElement(_uie.GetComponent<UIElement>().elementName, _uie.GetComponent<UIElement>().elementCategory, false);
    }
    public static void HexoItemOnClick(GameObject _gHexo)
    {
        SoundManager.THIS.PlaySound(SoundManager.THIS.acCheckDir);
        _gHexo.GetComponent<Hexo>().PlayAnimScale();
        //_gHexo.GetComponent<UIButton>().ExecuteClick(true);
    }
    #endregion

    #region Life
    public const string LIFE_KEY = GAME_KEY + "-LIFE-KEY";
    public const int MAX_LIFE = 5;
    public static void AddLife(int _totalLife) {
        int _lifeAdded = GetLife() + _totalLife;
        PlayerPrefs.SetInt(LIFE_KEY, (_lifeAdded >= MAX_LIFE ? MAX_LIFE : _lifeAdded));
        PlayerPrefs.Save();
    }
    public static void UsedLife() {
        AddLife(-1);
    }
    public static int GetLife() {
        return PlayerPrefs.GetInt(LIFE_KEY, 5);
    }

    #region Calculate Life by time
    public const string TIME_ADDED_LIFE_KEY = GAME_KEY + "-TIME-ADDED-LIFE-KEY";
    public static float GetDurationMinute()
    {
        string dateTimeString = PlayerPrefs.GetString(TIME_ADDED_LIFE_KEY);
        if (!string.IsNullOrEmpty(dateTimeString))
        {
            TimeSpan duration;
            DateTime timeFinish = System.DateTime.Parse(dateTimeString);
            duration = DateTime.Now - timeFinish;
            double totalsecond = duration.TotalMinutes;
            return (int)totalsecond;
        }
        else return 0;
    }
    public static void PutDateTime(DateTime dateInput)
    {
        PlayerPrefs.SetString(TIME_ADDED_LIFE_KEY, dateInput.ToString());
        PlayerPrefs.Save();
    }
    public static string GetStringDateTime(float duration)
    {
        TimeSpan ts = TimeSpan.FromMinutes(duration);

        string _strDay = "", _strHours = "", _strMinutes = "", _strSeconds = "";
        int _seconds = ts.Seconds;
        int _minutes = ts.Minutes;
        int _hours = ts.Hours;
        int _days = ts.Days;

        if (_days > 0)
        {
            _strDay = _days + "d";
        }
        if (_hours < 10)
        {
            _strHours = "0" + _hours + ":";
        }
        else
        {
            _strHours = _hours + ":";
        }

        if (_minutes < 10)
        {
            _strMinutes = "0" + _minutes + ":";
        }
        else
        {
            _strMinutes = _minutes + ":";
        }

        if (_seconds < 10)
        {
            _strSeconds = "0" + _seconds + "";
        }
        else
        {
            _strSeconds = _seconds + "";
        }

        //if (_minutes == 0) _strMinutes = "";

        string strResult = _strDay + _strHours + _strMinutes + _strSeconds;
        return strResult;
    }
    #endregion
    #endregion
}