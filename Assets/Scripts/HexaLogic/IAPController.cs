﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DoozyUI;

public class IAPController : MonoBehaviour {

    public static IAPController Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else {
            Destroy(gameObject);
        }
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void BuyHintsSuccess() {
        GameUtils.SetTotalHints(10);
        ProcessBuyHint();
    }
    private void ProcessBuyHint() {
        GameUtils.ShowElement(StartSceneContoller.Instance.gPanelMainButton);
        GameUtils.ShowElement(StartSceneContoller.Instance.gImgGameLogo);
        GameUtils.HideElement(StartSceneContoller.Instance.gPanelBuyHint);
    }
    public void BuyFailed() {
        ProcessBuyHint();
    }
    public void BuyRemoveAdsSuccess() {
        GameUtils.SetRemoveAds();
    }
}