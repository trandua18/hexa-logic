﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetMovement : MonoBehaviour {
    public Vector3 vDes;
    public float speed;
    [Range(150,10000)]
    public float speed_halley;
    public bool isLoop;
    public float curTime = 0;
    public bool isStartScreen;
    const float loopTime = 5;
    private Vector3 vCurDes;
	// Use this for initialization
	void Start () {
        vCurDes = transform.localPosition;
	}

    private void BeginMove() {
        if (Vector3.Distance(transform.localPosition, vDes) <= 1f) {
            transform.localPosition = vCurDes;
            if (isStartScreen) curTime = Random.Range(0, loopTime);
            else
                curTime = loopTime;
        }
        else
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, vDes, speed_halley * Time.deltaTime);
    }
	// Update is called once per frame
	void Update () {
        if (isLoop)
        {
            curTime -= Time.deltaTime;
            if (curTime <= 0) {
                BeginMove();
            }
        }
        else
        {
            if (Vector3.Distance(transform.localPosition, vDes) <= 1f)
            {
                transform.localPosition = vCurDes;
            }
            else
            {
                transform.localPosition = Vector3.MoveTowards(transform.localPosition, vDes, speed * Time.deltaTime);
            }
        }
	}
}
