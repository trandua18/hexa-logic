﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using System;

public class ShopController : MonoBehaviour/*, IStoreListener*/
{
    public static ShopController Instance;

    //public IStoreController m_StoreController;
    [HideInInspector]
    public GameUtils.ITEM_TYPE itemType;
    public GameObject gPanelShopContents;
    public GameObject gPanelConfirm;
    public IAPButton btnConfirmBuy;
    public Text txtPriceRemoveAds, txtPriceBuyHint, txtPriceBuyTurn, txtPriceBuyLife;

    public Text txtShopTitle, txtRemoveAdsTitle, txtRemoveAdsContent, txtBuyHintsTitle, txtBuyHintContent, txtExtraTitle, txtExtraContent, txtLifeTitle, txtLifeContent;

    public Text txtConfirmContent,txtConfirmTitle;

    private string pdNoAds, pdBuyHint, pdTurn, pdLife;

    private void Awake()
    {
        pdNoAds = "com.hexologic.noads";
        pdBuyHint = "com.hexologic.buyhints";
        pdTurn = "com.hexologic.extraturn";
        pdLife = "com.hexologic.extralife";

        //ConfigurationBuilder builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        //builder.AddProduct(pdBuyHint, ProductType.Consumable);
        //builder.AddProduct(pdLife, ProductType.Consumable);
        //builder.AddProduct(pdNoAds, ProductType.Consumable);
        //builder.AddProduct(pdTurn, ProductType.Consumable);
        //UnityPurchasing.Initialize(this, builder);

        Instance = this;
        gameObject.SetActive(false);
    }
    public void InitUIText() {
        txtShopTitle.text = GameUtils.dicLangInUse[GameUtils.SHOP_TITLE_KEY];
        txtRemoveAdsTitle.text = GameUtils.dicLangInUse[GameUtils.REMOVE_ADS_KEY];
        txtRemoveAdsContent.text = GameUtils.dicLangInUse[GameUtils.REMOVE_ADS_CONTENT];
        txtBuyHintsTitle.text = GameUtils.dicLangInUse[GameUtils.BUY_HINT_TITLE];
        txtBuyHintContent.text = GameUtils.dicLangInUse[GameUtils.BUY_HINT_CONTENT];
        txtExtraTitle.text = GameUtils.dicLangInUse[GameUtils.BUY_EXTRA_TURN_TITLE];
        txtExtraContent.text = GameUtils.dicLangInUse[GameUtils.BUY_EXTRA_TURN_CONTENT];
        txtLifeTitle.text = GameUtils.dicLangInUse[GameUtils.BUY_LIFE_TITLE];
        txtLifeContent.text = GameUtils.dicLangInUse[GameUtils.BUY_LIFE_CONTENT];

        txtConfirmContent.text = GameUtils.dicLangInUse[GameUtils.CONFIRM_CONTENT];
        txtConfirmTitle.text = GameUtils.dicLangInUse[GameUtils.CONFIRM_TITLE];
    }
    // Use this for initialization
    void Start () {
        //txtPriceRemoveAds.text = m_StoreController.products.WithID(pdNoAds).metadata.localizedPrice.ToString();
        //txtPriceBuyHint.text = m_StoreController.products.WithID(pdBuyHint).metadata.localizedPrice.ToString();
        //txtPriceBuyTurn.text = m_StoreController.products.WithID(pdTurn).metadata.localizedPrice.ToString();
        //txtPriceBuyLife.text = m_StoreController.products.WithID(pdLife).metadata.localizedPrice.ToString();

        InitUIText();
        //CloseConfirm();
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    public void BuyItem(ItemShopType _itemShop) {
        itemType = _itemShop.itemType;
        btnConfirmBuy.productId = _itemShop.productID;
        //txtConfirmContent.text = _itemShop.textContent;
        //gPanelConfirm.SetActive(true);
    }
    public void CloseShop() {
        gameObject.SetActive(false);
    }
    public void CloseConfirm() {
        gPanelConfirm.SetActive(false);
    }

    public void OnPurchaseSuccess() {
    }
    public void OnPurchaseFailed() {
        //Debug.LogError("Purchase Failed");
    }
    public Action<int> acAddHint;
    public Action<int> acAddLife;
    public Action acAddTurnLimit;
    public void BuyHintSuccess() {
        if (acAddHint != null) acAddHint(10);
    }
    public void BuyLifeSuccess() {
        if (acAddLife != null) acAddLife(GameUtils.MAX_LIFE);
    }
    public void BuyRemoveAdsSuccess() {
        GameUtils.SetRemoveAds();
    }
    public void BuyTurnExtraSuccess() {
        if (acAddTurnLimit != null) acAddTurnLimit();
    }

    #region Inplement
    //public void OnInitializeFailed(InitializationFailureReason error)
    //{
    //    Debug.LogError("OnInitializeFailed");
    //}

    //public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
    //{
    //    if (String.Equals(e.purchasedProduct.definition.id, pdBuyHint, StringComparison.Ordinal))
    //    {
    //        Debug.LogError("Add 10 hints");
    //        if (acAddHint != null) acAddHint(10);
    //    }
    //    if (String.Equals(e.purchasedProduct.definition.id, pdLife, StringComparison.Ordinal))
    //    {
    //        Debug.LogError("Add full life");
    //        if (acAddLife != null) acAddLife(GameUtils.MAX_LIFE);
    //    }
    //    if (String.Equals(e.purchasedProduct.definition.id, pdNoAds, StringComparison.Ordinal))
    //    {
    //        Debug.LogError("Remove ads");
    //        GameUtils.SetRemoveAds();
    //    }
    //    if (String.Equals(e.purchasedProduct.definition.id, pdTurn, StringComparison.Ordinal))
    //    {
    //        Debug.LogError("Add 20% turn limit");
    //        if (acAddTurnLimit != null) acAddTurnLimit();
    //    }

    //    CloseConfirm();
    //    return PurchaseProcessingResult.Complete;
    //}

    //public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
    //{
    //    CloseConfirm();
    //}

    //public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    //{
    //    m_StoreController = controller;
    //    Debug.LogError("OnInitialized");
    //}
    #endregion
}