﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MyDropDown : Dropdown, IPointerClickHandler
{
    //protected override void Start()
    //{
    //    Dropdown m_Dropdown = transform.GetComponentInChildren<Dropdown>();
    //    m_Dropdown = GetComponent<Dropdown>();
    //    m_Dropdown.ClearOptions();
        
    //    OptionData opDataEng = new OptionData("English");
    //    OptionData opDataChinese = new OptionData("Chinese");
    //    OptionData opDataJapan = new OptionData("Japan");
    //    OptionData opDataBrazil = new OptionData("Brazil");
    //    List<OptionData> lstData = new List<OptionData>();
    //    lstData.Add(opDataEng);
    //    lstData.Add(opDataChinese);
    //    lstData.Add(opDataJapan);
    //    lstData.Add(opDataBrazil);
    //    m_Dropdown.AddOptions(lstData);
    //}
    public override void OnPointerClick(PointerEventData eventData)
    {
        base.OnPointerClick(eventData);

        Dropdown dropdown = transform.GetComponentInChildren<Dropdown>();
        if (dropdown != null)
        {
            Canvas dropdownMenu = dropdown.GetComponentInChildren<Canvas>();
            if (dropdownMenu != null)
            {
                RectTransform ownerRectTransform = dropdown.GetComponent<RectTransform>();
                RectTransform dropdownRectTransform = dropdownMenu.GetComponent<RectTransform>();


                if (ownerRectTransform != null && dropdownRectTransform != null)
                {
                    float height = dropdownRectTransform.rect.height;
                    float ownerHeight = ownerRectTransform.rect.height;
                    dropdownRectTransform.offsetMax = new Vector2(dropdownRectTransform.offsetMax.x, -ownerHeight);
                    dropdownRectTransform.offsetMin = new Vector2(dropdownRectTransform.offsetMin.x, -(ownerHeight + height));
                }
            }
        }
    }
    protected override DropdownItem CreateItem(DropdownItem itemTemplate)
    {
        return base.CreateItem(itemTemplate);
    }
    protected override GameObject CreateDropdownList(GameObject template)
    {
        return base.CreateDropdownList(template);
    }
}
