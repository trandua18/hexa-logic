﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DoozyUI;

public class MenuController : MonoBehaviour
{
    public static MenuController THIS;
    public Text txtTotalStars;
    public Text txtTotalHints;
    public Transform trMaps;
    public ScrollRect scRect;
    public RectTransform rectContent;

    public GameObject gMapLevels;
    public GameObject gGamePlay;
    public GameObject gRingCurrentMap;
    public GameObject gEffectInMap;

    private Transform rectTarget;
    bool isMoveToLevel;
    Vector2 vDes;

    private void Awake()
    {
        THIS = this;
    }
    public void UpdateText()
    {
        txtTotalHints.text = "x " + GameUtils.GetTotalHints();
        txtTotalStars.text = "x " + GameUtils.GetTotalStars();
    }
    private void OnEnable()
    {
        UpdateText();

        for (int i = 0; i < trMaps.childCount; i++)
        {
            if (GameUtils.GAME_DEBUG) return;
            if (i <= GameUtils.GetCurrentLevel() - 1)
            {
                trMaps.GetChild(i).GetComponent<Button>().interactable = true;
            }
            else trMaps.GetChild(i).GetComponent<Button>().interactable = false;
        }
        GameManager.THIS._acSoundHanler += HandlerBGSound;
    }

    private void HandlerBGSound(bool isSoundOn)
    {
        if (isSoundOn) SoundManager.THIS.PlayBackgroundMusic(SoundManager.THIS.allInGameSound[SoundManager.THIS._indexBGSound]);
        else SoundManager.THIS.StopBackGroundMusic();
    }

    public void PlaySoundWhenClick()
    {
        SoundManager.THIS.ButtonClick();
    }
    // Use this for initialization
    void Start()
    {
        if (GameUtils.GetFinishTut())
        {
            if (GameUtils.isPlayTutorialMap)
            {
                scRect.enabled = false;
                gMapLevels.SetActive(false);
                gGamePlay.SetActive(true);
            }
            else
            {
                if (GameUtils.isGameRestart)
                {
                    HexaAnalytics.Instance.EVTutBegin();

                    GameUtils.isGameRestart = false;
                    InitStartGame(GameUtils.curLevel);
                }
            }
        }
        else
        {
            scRect.enabled = false;
            gMapLevels.SetActive(false);
            gGamePlay.SetActive(true);
        }
        SnapToLevel(GameUtils.GetCurrentLevel());
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!gGamePlay.activeSelf)
            {
                if (!GameManager.THIS.isQuitGame)
                {
                    GameManager.THIS.CallShowQuitPanel();
                }
                else
                {
                    GameManager.THIS.CloseQuitPanel();
                }
            }
            else
            {
                if (!GameManager.THIS.isGamePause)
                {
                    GameManager.THIS.CallShowPausePanel();
                }
                else
                {
                    GameUtils.HideElement(GameManager.THIS.gPanelPause);
                    GameManager.THIS.CallResumeGame();
                    GameUtils.ShowElement(GameManager.THIS.gPanelGrid);
                }
            }
        }


        if (Input.GetKeyDown(KeyCode.Space))
        {
            rectTarget = trMaps.GetChild(20);
            SnapTo(rectTarget);
            //scBar.value += 0.1f;
        }


        //if (!isMoveToLevel)
        //{
        //    StartCoroutine(MoveToLevel());
        //}
    }


    IEnumerator MoveToLevel()
    {
        yield return new WaitForSeconds(0.01f);
        rectTarget = trMaps.GetChild(/*GameUtils.GetCurrentLevel()*/22);
        isMoveToLevel = true;
        if (rectTarget != null)
            SnapTo(rectTarget);
    }
    public void SnapToLevel(int _level) {
        float _d = _level * 1.0f / GameUtils.MaxLevel;
        scRect.verticalNormalizedPosition = _d;
    }
    public void SmoothMove(float timeChange) {
        float _d = GameUtils.GetCurrentLevel() * 1.0f / GameUtils.MaxLevel;
        float _valueChange = Mathf.Lerp(scRect.verticalNormalizedPosition, _d, timeChange/* * Time.deltaTime*/);
        scRect.verticalNormalizedPosition = _valueChange;
    }
    private void SnapTo(Transform target)
    {
        //Canvas.ForceUpdateCanvases();
        //Vector2 vNew = (Vector2)scRect.transform.InverseTransformPoint(rectContent.position) - (Vector2)scRect.transform.InverseTransformPoint(target.position);
        //rectContent.anchoredPosition = /*new Vector2(vNew.x, rectContent.anchoredPosition.y);*/vNew;
        float _d = GameUtils.GetCurrentLevel() * 1.0f / GameUtils.MaxLevel;
        scRect.verticalNormalizedPosition = /*0.5f;*/ _d;
    }


    public void MapLevelClick(int levelIndex)
    {
        if (GameUtils.GetLife() > 0)
            InitStartGame(levelIndex);
        else {
            GameManager.THIS.gPanelShop.SetActive(true);
        }
        
    }

    public void InitStartGame(int _level)
    {
        if (_level <= GameUtils.MaxLevel)
        {
            GameUtils.curLevel = _level;
            InitPlayGame(true);
            HexaAnalytics.Instance.EVLevelStart(GameUtils.curLevel);
        }
        else if (GameUtils.curLevel + 1 <= GameUtils.MaxLevel)
        {
            GameUtils.curLevel += 1;
            InitPlayGame(true);
            HexaAnalytics.Instance.EVLevelStart(GameUtils.curLevel);
        }
        else
        {
            GameManager.THIS.ShowNotice(GameUtils.dicLangInUse[GameUtils.LEVEL_COMMING]);
        }
    }
    public void InitPlayGame(bool isActiveComponent)
    {
        scRect.enabled = !isActiveAndEnabled;
        gMapLevels.SetActive(!isActiveAndEnabled);
        gGamePlay.SetActive(isActiveAndEnabled);
        gRingCurrentMap.SetActive(!isActiveAndEnabled);
    }

    private bool isUranus, isJupiter, isSaturn;
    public void UranusClick(GameObject _g)
    {
        isUranus = !isUranus;
        //if (isUranus) GameUtils.ShowElement(_g);
        //else GameUtils.HideElement(_g);
    }
    public void JupiterClick(GameObject _g)
    {
        isJupiter = !isJupiter;
        //if (isJupiter) GameUtils.ShowElement(_g);
        //else GameUtils.HideElement(_g);
    }
    public void SaturnClick(GameObject _g)
    {
        isSaturn = !isSaturn;
        //if (isSaturn) GameUtils.ShowElement(_g);
        //else GameUtils.HideElement(_g);
    }
}