﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {
    public static SoundManager THIS;

    [HideInInspector]
    public AudioSource acSource;
    public AudioClip acMenuBG, acMainPlayBG, acInGame;
    public AudioClip acWin;
    public AudioClip acButtonClick;
    public AudioClip acNew;
    public AudioClip acFade;
    public AudioClip acFinish, acCheckDir, acHintClick;
    public AudioClip acFirework;
    public AudioClip acWarningTurnLimit;
    public AudioClip[] allInGameSound;
    public int _indexBGSound;

    private void Awake()
    {
        if (THIS == null)
        {
            THIS = this;
            DontDestroyOnLoad(this);
        }
        else Destroy(gameObject);
    }
    private void OnEnable()
    {
        _indexBGSound = Random.Range(0, allInGameSound.Length-1);
        acSource = GetComponent<AudioSource>();
    }
    public void ButtonClick() {
        if (GameUtils.GetSound()) {
            acSource.PlayOneShot(acButtonClick);
        }
    }
    public void PlaySound(AudioClip _ac) {
        if (GameUtils.GetSound())
        {
            acSource.PlayOneShot(_ac);
        }
    }
    public void PlayBackgroundMusic(AudioClip acBG) {
        if (GameUtils.GetSound())
        {
            if (acSource.isPlaying) acSource.Stop();
            acSource.clip = acBG;
            acSource.Play();
            acSource.loop = true;
        }
    }
    public void StopBackGroundMusic() {
        acSource.Stop();
    }
}