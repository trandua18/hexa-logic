﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using DG.Tweening;

public class StartSceneContoller : MonoBehaviour {
    public static StartSceneContoller Instance;

    public Text txtTotalStars;
    public GameObject gPanelMainButton;
    public GameObject gPanelSettings;
    public GameObject gPanelBuyHint;
    public GameObject gPanelRestore;
    public GameObject gImgGameLogo;
    public GameObject gTutNotify;

    public GameObject gSoundToggle;
    public Sprite sprSoundOn, sprSoundOff, sprToggleOn, sprToggleOff;
    public Text txtBuyHintTitle;
    public Text txtBuyHintContent;
    public Text txtRestoreTitle;
    public Text txtRestoreConfirm;
    public Text txtSetting;
    public Text txtThanks4Rate;
    public Text txtRateTitle;
    public Dropdown myDropdownMenu;
    public GameObject gLock;
    public Transform trChapter;
    public GameObject gLogo;
    public GameObject gClickEffect;
    public Sprite sprStar;
    public float timeDisplay;
    public Transform trRatingStars;
    public GameObject gNotiThanks4Rate, gPanelRate, gPanelShop, gPanelQuit;
    public Text txtQuitTitle, txtQuitContent;


    [HideInInspector]
    public int curChap = 0;


    private float timeActiveLogoAnim = 0;

    private void Awake()
    {
        Instance = this;
        GameUtils.InitLanguage();
        ChangeLanguage(GameUtils.GetCurLanguage());

        //gPanelShop.SetActive(true);
    }

    public void QuitGame() {
#if UNITY_EDITOR
        // Application.Quit() does not work in the editor so
        // UnityEditor.EditorApplication.isPlaying need to be set to false to end the game
        UnityEditor.EditorApplication.isPlaying = false;
#else
         Application.Quit();
#endif
    }
    public void BuyremoveAdsComplete() {
        HexaAnalytics.Instance.EVFinishBuyIAP("com.hexologic.noads", "start_screen", 0, 0);
        GameUtils.SetRemoveAds();
    }
    public void StarClick(int total) {
        CallShowRateGame();
        #region Insert Latter
        //if (total == 5)
        //{
        //    //Open Store
        //    CallShowRateGame();
        //    GameUtils.HideElement(gPanelRate);
        //}
        //else {
        //    //Show thanks Panel
        //    GameUtils.ShowElement(gNotiThanks4Rate);
        //    GameUtils.HideElement(gPanelRate);
        //}
        //for (int i = 0; i < total; i++) {
        //    trRatingStars.GetChild(i).GetComponent<Image>().sprite = sprStar;
        //}
        #endregion
    }
    #region Click Effects
    IEnumerator ClickEffects(Vector3 vDes) {
        gClickEffect.transform.position = new Vector3(vDes.x, vDes.y, gClickEffect.transform.position.z);
        gClickEffect.SetActive(true);
        yield return new WaitForSeconds(timeDisplay);
        gClickEffect.SetActive(false);
    }
    #endregion
    private void UpdateText() {
        txtTotalStars.text = GameUtils.GetTotalStars() + GameUtils.dicLangInUse[GameUtils.STARS];
        txtBuyHintTitle.text = GameUtils.dicLangInUse[GameUtils.BUY_HINT_TITLE].ToUpper();
        txtBuyHintContent.text = GameUtils.dicLangInUse[GameUtils.BUY_10_HINTS];
        txtRestoreTitle.text = GameUtils.dicLangInUse[GameUtils.RESTORE_TITLE].ToUpper();
        txtRestoreConfirm.text = GameUtils.dicLangInUse[GameUtils.ARE_YOU_SURE];
        txtSetting.text = GameUtils.dicLangInUse[GameUtils.SETTING_TEXT].ToUpper();
        txtThanks4Rate.text = GameUtils.dicLangInUse[GameUtils.THANKS_4_RATE];

        txtQuitTitle.text = GameUtils.dicLangInUse[GameUtils.QUIT_GAME].ToUpper();
        txtQuitContent.text = GameUtils.dicLangInUse[GameUtils.QUIT_CONTENT];
    }
    private void OnEnable()
    {
        UpdateText();

        SetSound();
        SetGGLogin();

        SoundManager.THIS.PlayBackgroundMusic(SoundManager.THIS.acMainPlayBG);

        InitChapterSelected(GameUtils.GetChapSelected());


    }
    private void HandleBuyHintEvent(int _total) {
        GameUtils.SetTotalHints(_total);
    }
    private void HandleBuyExtraTurn() {
        GameUtils.SetBuyExtraTurn();
    }
    private void HandleBuyLife(int _total) {
        GameUtils.AddLife(_total);
    }

    public void DropdownChangeValue() {
        ChangeLanguage(myDropdownMenu.value);
    }

    private void InitChapterSelected(int _curChap) {
        if (_curChap <= GameUtils.GetChapUnlock()) gLock.SetActive(false);
        else gLock.SetActive(true);
        for (int i = 0; i < trChapter.childCount; i++)
        {
            if (i == _curChap)
            {
                trChapter.GetChild(i).gameObject.SetActive(true);
            }
            else trChapter.GetChild(i).gameObject.SetActive(false);
        }
    }
    public void Call_NextMap() {
        if (curChap < trChapter.childCount)
        {
            curChap++;
            if (curChap >= trChapter.childCount) curChap = 0;
            InitChapterSelected(curChap);
        }
    }
    public void Call_ForwardMap() {
        curChap--;
        if (curChap < 0) curChap = trChapter.childCount - 1;

        InitChapterSelected(curChap);
    }

    private void OnDisable()
    {
        ShopController.Instance.acAddHint -= HandleBuyHintEvent;
        ShopController.Instance.acAddLife -= HandleBuyLife;
        ShopController.Instance.acAddTurnLimit -= HandleBuyExtraTurn;
    }

    public void BuyHintSuccess() {
        HexaAnalytics.Instance.EVFinishBuyIAP("com.hexologic.buyhints", "start_screen", 0, 0);
        gPanelMainButton.SetActive(true);
        gPanelBuyHint.SetActive(false);
    }

    private void SetSound() {
        if (GameUtils.GetSound())
        {
            gSoundToggle.GetComponent<Image>().sprite = sprToggleOn;
            SoundManager.THIS.PlayBackgroundMusic(SoundManager.THIS.acMainPlayBG);
        }
        else
        {
            gSoundToggle.GetComponent<Image>().sprite = sprToggleOff;
            SoundManager.THIS.StopBackGroundMusic();
        }
    }

    private void SetGGLogin()
    {
        if (GameUtils.GetGGLogin())
        {
        }
        else
        {
        }
    }
    
    // Use this for initialization
    void Start() {

        ShopController.Instance.acAddHint += HandleBuyHintEvent;
        ShopController.Instance.acAddLife += HandleBuyLife;
        ShopController.Instance.acAddTurnLimit += HandleBuyExtraTurn;
    }

    // Update is called once per frame
    void Update() {

        if (Input.GetMouseButtonDown(0)) {
            Vector3 _vDes = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            StartCoroutine(ClickEffects(_vDes));
        }
        if (Input.GetKeyDown(KeyCode.Escape)) {
            gPanelShop.SetActive(false);
            GameUtils.ShowElement(gPanelQuit);
        }
    }

    public void CallSetSoundOn(GameObject gToggle) {
        GameUtils.SetSound();
        SetSound();
    }
    public void CallSetGoogleOn(GameObject gToggle) {
        GameUtils.SetGGLogin();
        SetGGLogin();
    }
    public void CallShowSettingPanel() {
        gPanelMainButton.SetActive(false);
        gPanelSettings.SetActive(true);
    }
    #region Restore
    public void CallRestorePurchase() {
    }
    public void PlaySoundWhenClick() {
        SoundManager.THIS.ButtonClick();
    }
    public void CallShowRestorePanel() {
        gPanelMainButton.SetActive(false);
        gPanelRestore.SetActive(true);
    }
    #endregion
    public void CallShowMenuGame() {
        SplashScreen.Instance.LoadSene(GameUtils.SCENE_MAIN_MENU);
    }
    public void CallStartTutorial() {
        HexaAnalytics.Instance.EVTutBegin();
        SoundManager.THIS.ButtonClick();

        if (!GameUtils.GetFinishTut() || !GameUtils.isPlayTutorialMap)
        {
            GameUtils.isPlayTutorialMap = true;
            SceneManager.LoadSceneAsync(GameUtils.SCENE_MAIN_MENU);
        }
        else {
            gTutNotify.transform.GetChild(0).GetComponent<Text>().text = GameUtils.dicLangInUse[GameUtils.COMPLETE_TUT];
            GameUtils.ShowElement(gTutNotify);
        }
    }
    public void ChangeLanguage(int _index) {
        switch (_index) {
            case 0:
                GameUtils.dicLangInUse = GameUtils.dicUSA;
                break;
            case 1:
                GameUtils.dicLangInUse = GameUtils.dicCN;
                break;
            case 2:
                GameUtils.dicLangInUse = GameUtils.dicBRZ;
                break;
            case 3:
                GameUtils.dicLangInUse = GameUtils.dicJAP;
                break;
        }
        GameUtils.SetCurLanguage(_index);
        UpdateText();
    }
    public void CallShowBuyHint() {
        gPanelMainButton.SetActive(false);
        gPanelBuyHint.SetActive(true);
    }
    public void CallPurchaseHint() {
    }
    public void CallPurchaseRemoveAds() {
    }
    public void CallShowRateGame() {
        //Check Rate Game with 5*
        Application.OpenURL("market://details?id=" + GameUtils.GAME_PACK);
    }
    public void HidePanel(GameObject g) {
        gPanelMainButton.SetActive(true);

        gPanelSettings.SetActive(false);
        gPanelBuyHint.SetActive(false);
        gPanelRestore.SetActive(false);
    }

    public IEnumerator HideAllPanel() {
        yield return new WaitForSeconds(0.1f);
        gPanelMainButton.SetActive(true);

        gPanelSettings.SetActive(false);
        gPanelBuyHint.SetActive(false);
        gPanelRestore.SetActive(false);
    }

    public void CallShowShop() {
        ShopController.Instance.InitUIText();
        gPanelShop.SetActive(true);
        //GameUtils.ShowElement(gPanelShop);
    }
}