﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelDots : MonoBehaviour
{
    public static LevelDots THIS;
    public Sprite sprReplay, sprComplete, sprIsReady, sprLock, sprLinkComplete;
    public Sprite sprLine;
    public GameObject _myLine;


    float timeBlink = 2;
    bool canBlink = false;
    int _level;


    private void Awake()
    {
        THIS = this;
    }
    private void OnEnable()
    {
        _level = int.Parse(gameObject.name);

        if (_level < GameUtils.GetCurrentLevel())
        {
            transform.GetChild(0).GetChild(0).gameObject.SetActive(false);
            transform.GetChild(0).GetComponent<Image>().sprite = sprReplay;
        }
        else if (_level != GameUtils.GetCurrentLevel())
        {
            transform.GetChild(0).GetComponent<Image>().sprite = sprLock;
        }
    }
    public void InitLevelSelected()
    {
        for (int i = 0; i < MenuController.THIS.trMaps.childCount; i++)
        {
            if (i == GameUtils.GetCurrentLevel())
            {
                MenuController.THIS.gRingCurrentMap.transform.SetParent(MenuController.THIS.trMaps.GetChild(i), false);
            }
           
            if (i <= GameUtils.GetCurrentLevel())
            {
                MenuController.THIS.trMaps.GetChild(i).GetComponent<Button>().interactable = true;
            }
            else {
                MenuController.THIS.trMaps.GetChild(i).GetComponent<Button>().interactable = false;

                
            }
            if (i <= GameUtils.GetCurrentLevel())
            {
                MenuController.THIS.trMaps.GetChild(i - 1 <= 0 ? 0 : i - 1).GetChild(0).GetChild(1).GetComponent<LineRenderer>().startColor = Color.green;
                MenuController.THIS.trMaps.GetChild(i - 1 <= 0 ? 0 : i - 1).GetChild(0).GetChild(1).GetComponent<LineRenderer>().endColor = Color.green;
            }
            else
            {
                MenuController.THIS.trMaps.GetChild(i - 1 <= 0 ? 0 : i - 1).GetChild(0).GetChild(1).GetComponent<LineRenderer>().startColor = Color.white;
                MenuController.THIS.trMaps.GetChild(i - 1 <= 0 ? 0 : i - 1).GetChild(0).GetChild(1).GetComponent<LineRenderer>().endColor = Color.white;
            }
        }
    }

    // Use this for initialization
    void Start()
    {
        if (_level < SplashScreen.Instance.maxLevel)
            DrawLine2Level();

        if (_level == GameUtils.GetCurrentLevel())
        {
            MenuController.THIS.gRingCurrentMap.transform.SetParent(transform, false);
        }

        for (int i = 0; i < MenuController.THIS.trMaps.childCount; i++)
        {
            if (i <= GameUtils.GetCurrentLevel() - 1)
            {
                MenuController.THIS.trMaps.GetChild(i - 1 <= 0 ? 0 : i - 1).GetChild(0).GetChild(1).GetComponent<LineRenderer>().startColor = Color.green;
                MenuController.THIS.trMaps.GetChild(i - 1 <= 0 ? 0 : i - 1).GetChild(0).GetChild(1).GetComponent<LineRenderer>().endColor = Color.green;
            }
            else
            {
                MenuController.THIS.trMaps.GetChild(i - 1 <= 0 ? 0 : i - 1).GetChild(0).GetChild(1).GetComponent<LineRenderer>().startColor = Color.white;
                MenuController.THIS.trMaps.GetChild(i - 1 <= 0 ? 0 : i - 1).GetChild(0).GetChild(1).GetComponent<LineRenderer>().endColor = Color.white;
            }
        }
    }

    Vector3 vStart;
    Vector3 vDes;
    private void DrawLine2Level()
    {
        _myLine.SetActive(true);
        _myLine.GetComponent<LineRenderer>().positionCount = 2;
        vStart = transform.position;
        vDes = _myLine.transform.InverseTransformPoint(MenuController.THIS.trMaps.GetChild(_level).GetChild(0).position);
        _myLine.GetComponent<LineRenderer>().SetPosition(0, vStart);
        _myLine.GetComponent<LineRenderer>().SetPosition(1, vDes);
    }

    IEnumerator Blink()
    {
        gameObject.GetComponent<Image>().enabled = false;
        yield return new WaitForSeconds(timeBlink);
        gameObject.GetComponent<Image>().enabled = true;
    }
}
