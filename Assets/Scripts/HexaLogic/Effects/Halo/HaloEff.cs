﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HaloEff : MonoBehaviour {

    public bool isLeft;
    public float speed;
	// Use this for initialization
	void Start () {
	}
	void Update () {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + (isLeft ? -1 : 1) * speed * Time.deltaTime);
    }
}