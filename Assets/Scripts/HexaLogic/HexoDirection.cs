﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexoDirection : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    private void OnMouseDown()
    {
        if (gameObject.activeSelf) {
            foreach (GameObject gHexoObj in MapCreate.Instance.dicAllHexaObject.Values) {
                Hexo _hexo = gHexoObj.GetComponent<Hexo>();
                if (!_hexo.isDirection) {
                    if (_hexo.keys.Contains(transform.parent.parent.name)) {
                        GameUtils.HexoItemOnClick(gHexoObj/*.transform.GetChild(1).gameObject*/);
                    }
                }
            }
        }
    }
}
