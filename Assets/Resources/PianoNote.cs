﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoNote : MonoBehaviour {
    public float semitone_offset = 0;
    AudioSource audio;
    // Use this for initialization
    private void OnEnable()
    {
        audio = GetComponent<AudioSource>();
    }
    void Start() {
    }

    // Update is called once per frame
    void Update() {

    }

    void OnMouseDown()
    {
        //PlayNote();
        //PlayNote(-4);
        //PlayNote(4);
    }

    void OnCollisionEnter()
    {
        //PlayNote();
    }
    void PlayNote(int note)
    {
        audio.pitch = Mathf.Pow(2f, note / 12.0f);
        audio.Play();
    }
    public void PlayNote()
    {
        audio.pitch =/* Random.Range(0.5f, 1.5f);*/ Mathf.Pow(2f, semitone_offset / 12.0f);
        audio.Play();
    }
}
