﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PianoNoteManager : MonoBehaviour {
    public float timeDisplay;

    public List<GameObject> lstPianoNote;
    public GameObject gNote1, gNote2, gNote3;
    public GameObject gStarAnim;
    
    public void ButtonOne() {
        _indexNote = 0;
        StartCoroutine(DisplayFunction(gNote1.transform));
        gStarAnim.SetActive(true);
        starFxController.myStarFxController.ea = 1;
        //DisplayNote(gNote1.transform, _indexNote);
    }
    public void ButtonTwo() {
        _indexNote = 0;
        StartCoroutine(DisplayFunction(gNote2.transform));
        gStarAnim.SetActive(true);
        starFxController.myStarFxController.ea = 2;
        //DisplayNote(gNote2.transform, _indexNote);
    }
    public void ButtonThree() {
        _indexNote = 0;
        StartCoroutine(DisplayFunction(gNote3.transform));
        gStarAnim.SetActive(true);
        starFxController.myStarFxController.ea = 3;
        //DisplayNote(gNote3.transform, _indexNote);
    }
    IEnumerator DisplayFunction(Transform tr) {
        while (_indexNote < tr.childCount) {
            tr.GetChild(_indexNote).gameObject.SetActive(true);
            tr.GetChild(_indexNote).gameObject.GetComponent<PianoNote>().PlayNote();
            yield return new WaitForSeconds(timeDisplay);
            _indexNote++;
        }
    }
    int _indexNote;

    void DisplayNote(Transform tr,int _index) {
        if (_index < tr.childCount)
        {
            tr.GetChild(_index).gameObject.SetActive(true);
            tr.GetChild(_index).gameObject.GetComponent<PianoNote>().PlayNote();
            _index++;
            DisplayNote(tr, _index);
        }
    }
}