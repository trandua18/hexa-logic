{
  "LevelID": "",
  "HintsContainers": [
    {
      "IdX": 12,
      "IdY": 14,
      "idHInt": 12013,
      "HintsData": [
        {
          "Direction": 3,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 13,
      "IdY": 12,
      "idHInt": 12013,
      "HintsData": [
        {
          "Direction": 5,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 13,
      "IdY": 11,
      "idHInt": 12012,
      "HintsData": [
        {
          "Direction": 5,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 11,
      "IdY": 11,
      "idHInt": 10012,
      "HintsData": [
        {
          "Direction": 5,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 10,
      "IdY": 11,
      "idHInt": 9012,
      "HintsData": [
        {
          "Direction": 5,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 7,
      "IdY": 13,
      "idHInt": 8013,
      "HintsData": [
        {
          "Direction": 1,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 7,
      "IdY": 14,
      "idHInt": 8014,
      "HintsData": [
        {
          "Direction": 1,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 8,
      "IdY": 15,
      "idHInt": 8014,
      "HintsData": [
        {
          "Direction": 3,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 9,
      "IdY": 15,
      "idHInt": 9014,
      "HintsData": [
        {
          "Direction": 3,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 10,
      "IdY": 15,
      "idHInt": 10014,
      "HintsData": [
        {
          "Direction": 3,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 11,
      "IdY": 15,
      "idHInt": 11014,
      "HintsData": [
        {
          "Direction": 3,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 8,
      "IdY": 12,
      "idHInt": 9012,
      "HintsData": [
        {
          "Direction": 1,
          "IsActive": true
        }
      ]
    }
  ],
  "Title": "GameTitle",
  "TurnLimited": 0,
  "IsProgressive": false,
  "Fields": [
    {
      "IdX": 9,
      "IdY": 13,
      "CurrentValue": 0,
      "TargetValue": 4,
      "IsLocked": true,
      "DependentId": 0,
      "myKey": "9-13",
      "HintsData": null,
      "lstDir": [],
      "keys": []
    },
    {
      "IdX": 11,
      "IdY": 13,
      "CurrentValue": 0,
      "TargetValue": 4,
      "IsLocked": true,
      "DependentId": 0,
      "myKey": "11-13",
      "HintsData": null,
      "lstDir": [],
      "keys": []
    },
    {
      "IdX": 8,
      "IdY": 14,
      "CurrentValue": 0,
      "TargetValue": 1,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "8-14",
      "HintsData": null,
      "lstDir": [
        1,
        3
      ],
      "keys": []
    },
    {
      "IdX": 11,
      "IdY": 12,
      "CurrentValue": 0,
      "TargetValue": 1,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "11-12",
      "HintsData": null,
      "lstDir": [],
      "keys": []
    },
    {
      "IdX": 10,
      "IdY": 14,
      "CurrentValue": 0,
      "TargetValue": 1,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "10-14",
      "HintsData": null,
      "lstDir": [
        3
      ],
      "keys": []
    },
    {
      "IdX": 11,
      "IdY": 14,
      "CurrentValue": 0,
      "TargetValue": 1,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "11-14",
      "HintsData": null,
      "lstDir": [
        3
      ],
      "keys": []
    },
    {
      "IdX": 9,
      "IdY": 12,
      "CurrentValue": 0,
      "TargetValue": 1,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "9-12",
      "HintsData": null,
      "lstDir": [
        5,
        1
      ],
      "keys": []
    },
    {
      "IdX": 10,
      "IdY": 12,
      "CurrentValue": 0,
      "TargetValue": 3,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "10-12",
      "HintsData": null,
      "lstDir": [
        5
      ],
      "keys": []
    },
    {
      "IdX": 10,
      "IdY": 13,
      "CurrentValue": 0,
      "TargetValue": 3,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "10-13",
      "HintsData": null,
      "lstDir": [],
      "keys": []
    },
    {
      "IdX": 9,
      "IdY": 14,
      "CurrentValue": 0,
      "TargetValue": 3,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "9-14",
      "HintsData": null,
      "lstDir": [
        3
      ],
      "keys": []
    },
    {
      "IdX": 8,
      "IdY": 13,
      "CurrentValue": 0,
      "TargetValue": 2,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "8-13",
      "HintsData": null,
      "lstDir": [
        1
      ],
      "keys": []
    },
    {
      "IdX": 12,
      "IdY": 12,
      "CurrentValue": 0,
      "TargetValue": 2,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "12-12",
      "HintsData": null,
      "lstDir": [
        5
      ],
      "keys": []
    },
    {
      "IdX": 12,
      "IdY": 13,
      "CurrentValue": 0,
      "TargetValue": 2,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "12-13",
      "HintsData": null,
      "lstDir": [
        3,
        5
      ],
      "keys": []
    }
  ]
}