{
  "LevelID": "",
  "HintsContainers": [
    {
      "IdX": 8,
      "IdY": 14,
      "HintsData": [
        {
          "Direction": 1,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 7,
      "IdY": 16,
      "HintsData": [
        {
          "Direction": 2,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 10,
      "IdY": 16,
      "HintsData": [
        {
          "Direction": 3,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 14,
      "IdY": 12,
      "HintsData": [
        {
          "Direction": 5,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 12,
      "IdY": 16,
      "HintsData": [
        {
          "Direction": 3,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 11,
      "IdY": 12,
      "HintsData": [
        {
          "Direction": 0,
          "IsActive": true
        }
      ]
    },
    {
      "IdX": 9,
      "IdY": 12,
      "HintsData": [
        {
          "Direction": 0,
          "IsActive": true
        }
      ]
    }
  ],
  "Title": "GameTitle",
  "TurnLimited": 0,
  "IsProgressive": false,
  "Fields": [
    {
      "IdX": 10,
      "IdY": 14,
      "CurrentValue": 0,
      "TargetValue": 3,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "10-14",
      "HintsData": null,
      "keys": []
    },
    {
      "IdX": 11,
      "IdY": 14,
      "CurrentValue": 0,
      "TargetValue": 3,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "11-14",
      "HintsData": null,
      "keys": []
    },
    {
      "IdX": 9,
      "IdY": 14,
      "CurrentValue": 0,
      "TargetValue": 2,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "9-14",
      "HintsData": null,
      "keys": []
    },
    {
      "IdX": 12,
      "IdY": 14,
      "CurrentValue": 0,
      "TargetValue": 1,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "12-14",
      "HintsData": null,
      "keys": []
    },
    {
      "IdX": 12,
      "IdY": 15,
      "CurrentValue": 0,
      "TargetValue": 1,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "12-15",
      "HintsData": null,
      "keys": []
    },
    {
      "IdX": 9,
      "IdY": 13,
      "CurrentValue": 0,
      "TargetValue": 3,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "9-13",
      "HintsData": null,
      "keys": []
    },
    {
      "IdX": 8,
      "IdY": 15,
      "CurrentValue": 0,
      "TargetValue": 2,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "8-15",
      "HintsData": null,
      "keys": []
    },
    {
      "IdX": 13,
      "IdY": 13,
      "CurrentValue": 0,
      "TargetValue": 3,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "13-13",
      "HintsData": null,
      "keys": []
    },
    {
      "IdX": 10,
      "IdY": 15,
      "CurrentValue": 0,
      "TargetValue": 1,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "10-15",
      "HintsData": null,
      "keys": []
    },
    {
      "IdX": 11,
      "IdY": 13,
      "CurrentValue": 0,
      "TargetValue": 1,
      "IsLocked": false,
      "DependentId": 0,
      "myKey": "11-13",
      "HintsData": null,
      "keys": []
    }
  ]
}