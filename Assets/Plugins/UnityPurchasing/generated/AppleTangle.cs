#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("LmFoLnpmay56ZmtgLm9+fmJnbW9sYmsufXpvYGpvfGouemt8Y30ubyIubWt8emdoZ21vemsufmFiZ213dy5vfX17Y2t9Lm9tbWt+em9gbWt6Z2hnbW96ay5sdy5vYHcufm98egoIHQxbXT8dPh8IDVsKBB0ET35+AZMz/SVHJhTG8MC7twDXUBLYxTMRi42LFZczSTn8p5VOgCLav54c1oUXh9D3RWL7CaUsPgzmFjD2XgfdAwgHJIhGiPkDDw8LCw4NjA8PDlJ+YmsuTWt8emdoZ21vemdhYC5Pe7kVs51MKhwkyQETuEOSUG3GRY4ZOJdCI3a544KV0v15lfx43Hk+Qc89OFQ+bD8FPgcIDVsKCB0MW10/HWdoZ21vemdhYC5Pe3pmYXxnenc/dD6MD3g+AAgNWxMBDw/xCgoNDA9LcBFCZV6YT4fKemwFHo1PiT2Ejyg+KggNWwoFHRNPfn5iay5Na3x6ptJwLDvEK9vXAdhl2qwqLR/5r6IGJQgPCwsJDA8YEGZ6en59NCEheXxvbXpnbWsufXpvemtja2B6fSA+1zhxz4lb16mXtzxM9dbbf5Bwr1ylrX+cSV1bz6EhT7329e1+w+itQgniczeNhV0u3TbKv7GUQQRl8SXyPh8IDVsKBB0ET35+YmsuR2BtID9+YmsuXGFhei5NTz4QGQM+OD46PCBOqPlJQ3EGUD4RCA1bEy0KFj4Ym5B0AqpJhVXaGDk9xcoBQ8AaZ98YPhoIDVsKDR0DT35+YmsuXGFhenpmYXxnenc/GD4aCA1bCg0dA09+Cw4NjA8BDj6MDwQMjA8PDuqfpwc7PD86Pj04VBkDPTs+PD43PD86PmJrLkdgbSA/KD4qCA1bCgUdE09+YGoubWFgamd6Z2FgfS5haC57fWu7NKP6AQAOnAW/LxggetsyA9VsGHFPppb338RokiplH96tteoVJM0RMyhpLoQ9ZPkDjMHQ5a0h911kVWoq7OXfuX7RAUvvKcT/Y3bj6bsZGS5NTz6MDyw+AwgHJIhGiPkDDw8Pzm09efk0CSJY5dQBLwDUtH0XQbskiEaI+QMPDwsLDj5sPwU+BwgNW1xrYmdvYG1rLmFgLnpmZ30ubWt8R9Z4kT0aa695mscjDA0PDg+tjA8Rn9UQSV7lC+NQd4oj5TisWUJb4mmBBrou+cWiIi5hfrgxDz6CuU3BeXkgb35+YmsgbWFjIW9+fmJrbW+OGiXeZ0maeAfw+mWDIE6o+UlDcWo7LRtFG1cTvZr5+JKQwV60z1ZeCA1bEwAKGAoaJd5nSZp4B/D6ZYMGUD6MDx8IDVsTLgqMDwY+jA8KPowPDggHJIhGiPltagsPPo/8PiQIvz5W4lQKPIJmvYET0Gt98WlQa7Iub2BqLm1rfHpnaGdtb3pnYWAufrD6fZXg3GoBxXdBOtasMPd28WXGPowKtT6MDa2uDQwPDAwPDD4DCAeBfY9uyBVVByGcvPZKRv5uNpAb+8cXfPtTANtxUZX8Kw20W4FDUwP/V6kLB3IZTlgfEHrduYUtNUmt22EhPo/NCAYlCA8LCwkMDD6PuBSPvQg+AQgNWxMdDw/xCgs+DQ8P8T4TXqSE29Tq8t4HCTm+e3sv");
        private static int[] order = new int[] { 41,42,44,40,39,44,22,49,16,47,39,59,48,16,59,28,25,39,54,35,46,58,24,51,47,47,31,54,48,44,59,49,40,43,51,39,56,53,47,57,42,47,54,48,58,56,54,54,59,59,51,59,56,59,54,55,57,57,59,59,60 };
        private static int key = 14;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
