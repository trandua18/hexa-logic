#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("Z/RG5MjXJXh4/yvb3nGlWq/VIzJRrvJ8gR9J4RdarwYtW1CtRfO/qmYoU2O7VW4v68V7ZsRCw0rQO+nMPuD56RkhY9ddM/aTseKFgN/8QmCdpcZ6UxUXKgua8Hudqf2azfyfDsHo6iDiJJKxHlNiNgxnVIjCdrPrOYsIKzkEDwAjj0GP/gQICAgMCQpqJkK+d4xqMU1nnP1QsK6nF3ziyosIBgk5iwgDC4sICAm+ZYMB2HJ/QJsdoATFk072zLfKSCH+dV1eo6h1SWnE4LfbP3gp/RGhBnKwCPDGC0mkaw5p2sfMPt+ZnhpudcvFXRKNEgx+BWTpjeEJwYRkQDc6tavKT2mYTV73x6vi2VMSMBK2/lSohey0PyKFRtETQyjtkgsKCAkI");
        private static int[] order = new int[] { 5,8,7,9,9,7,7,13,8,11,11,12,13,13,14 };
        private static int key = 9;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
